﻿using NotEnoughMana.ExtensionMethod;
using UnityEngine;

namespace NotEnoughMana.Projectile
{
    public class LinearMove : MonoBehaviour
    {
        public Vector2 Velocity;

        private void Update()
        {
            transform.position += Velocity.XY0() * Time.deltaTime;
        }
    }
}
