﻿using UnityEngine;
using NotEnoughMana.QuickCollision;

namespace NotEnoughMana.Projectile
{
    public sealed class Lifetime : MonoBehaviour
    {
        public float MaxLifeTime;
        public float ColliderLifetime;
        private float timer;
        public void SetLifetime(float lifetime, float cLifetime = 0 )
        {
            MaxLifeTime = lifetime;
            if (cLifetime != 0)
                ColliderLifetime = cLifetime;
            else
                ColliderLifetime = MaxLifeTime;
        }
        private void Update()
        {
            timer += Time.deltaTime;
            if (timer > ColliderLifetime)
                gameObject.GetComponent<QuickCircleCollider>().Active = false;
            if (timer > MaxLifeTime)
                GameObject.Destroy(this.gameObject);
        }
    }
}
