﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NotEnoughMana.Control;

namespace NotEnoughMana.UI
{
    public class UIManager : MonoBehaviour
    {
        private static UIManager instance;
        public static UIManager Instance
        {
            get
            {
                if (!instance)
                {
                    GameObject container = new GameObject();
                    container.name = "UIManagerContainer";
                    instance = container.AddComponent(typeof(UIManager)) as UIManager;
                }
                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        // Use this for initialization
        void Start()
        {
            Debug.Log("UI Init");
            _mainWeapon = GameObject.Find("Sprite_MainWeapon").GetComponent<Image>();
            _subWeapon = GameObject.Find("Sprite_SubWeapon").GetComponent<Image>();
            _acc = GameObject.Find("Sprite_Acc").GetComponent<Image>();
            _chain = GameObject.Find("ChainText").GetComponent<Text>();
            _player = GameObject.Find("Player").GetComponent<PlayerScript>();
            _bossbar = GameObject.Find("bosshp");
            _bossbar.SetActive(false);
            SetMainWeapon(_player.CurrentWeapon);
            SetSubWeapon(_player.SpareWeapon);
            SetAccessory(_player.CurrentRing);
        }
        public void ShakeCamera(float dur, float mag)
        {
            _shakeTime = dur;
            _mag = mag;
        }
        // Update is called once per frame
        void Update()
        {
            if (_shakeTime > 0)
            {

                _shakeTime -= Time.deltaTime;
                Vector3 shake = new Vector3(Random.Range(-_mag, _mag), Random.Range(-_mag, _mag), -10f);
                //Camera.main.transform.position = shake;
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, shake, 0.1f);
            }
            else
            {
                //Look towards the targeter
                //Camera.main.transform.position = new Vector3(0, 0, -10);
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(0, 0, -10), 0.1f);

            }


        }
        public void Init()
        {
            _shakeTime = 0f;
        }
        public void ResetChain()
        {
            _chain.text = "";
        }
        public void UpdateChain(int chain)
        {
            _chain.text = chain + "Chain!";
        }
        public void SetMainWeapon(WeaponEnum wp)
        {
            switch (wp)
            {
                case WeaponEnum.Basic:
                    _mainWeapon.sprite = Resources.Load<Sprite>("itemslot_basic");
                    break;
                case WeaponEnum.ShadowNova:
                    _mainWeapon.sprite = Resources.Load<Sprite>("itemslot_shadow");
                    break;

                case WeaponEnum.FrozenOculus:
                    _mainWeapon.sprite = Resources.Load<Sprite>("itemslot_ice");
                    break;

                case WeaponEnum.VividBead:
                    _mainWeapon.sprite = Resources.Load<Sprite>("itemslot_vivid");
                    break;

                case WeaponEnum.InfernoStaff:
                    _mainWeapon.sprite = Resources.Load<Sprite>("itemslot_inferno");
                    break;

                default:
                    _mainWeapon.sprite = Resources.Load<Sprite>("Empty");
                    break;
            }
        }
        public void SetSubWeapon(WeaponEnum wp)
        {
            switch (wp)
            {
                case WeaponEnum.Basic:
                    _subWeapon.sprite = Resources.Load<Sprite>("itemslot_basic");
                    break;
                case WeaponEnum.ShadowNova:
                    _subWeapon.sprite = Resources.Load<Sprite>("itemslot_shadow");
                    break;

                case WeaponEnum.FrozenOculus:
                    _subWeapon.sprite = Resources.Load<Sprite>("itemslot_ice");
                    break;

                case WeaponEnum.VividBead:
                    _subWeapon.sprite = Resources.Load<Sprite>("itemslot_vivid");
                    break;

                case WeaponEnum.InfernoStaff:
                    _subWeapon.sprite = Resources.Load<Sprite>("itemslot_inferno");
                    break;
                default:
                    _subWeapon.sprite = Resources.Load<Sprite>("Empty");
                    break;
            }

        }
        public void SetAccessory(RingEnum acc)
        {
            switch (acc)
            {
                case RingEnum.BlackholeRing:
                    _acc.sprite = (Sprite)Resources.Load("item_bracelet_black", typeof(Sprite));
                    break;
                case RingEnum.RefresherAmulet:
                    _acc.sprite = (Sprite)Resources.Load("item_Refresh", typeof(Sprite));
                    break;
                case RingEnum.TurningTide:
                    _acc.sprite = (Sprite)Resources.Load("item_bracelet", typeof(Sprite));
                    break;
                case RingEnum.LivingShadow:
                    _acc.sprite = (Sprite)Resources.Load("item_livingshadow", typeof(Sprite));
                    break;
                case RingEnum.LocusOfPower:
                    _acc.sprite = (Sprite)Resources.Load("item_necklace", typeof(Sprite));
                    break;
                default:
                    _acc.sprite = Resources.Load<Sprite>("Empty");
                    break;
            }
        }

        private Image _mainWeapon;
        private Image _subWeapon;
        private Image _acc;
        private Text _chain;
        private PlayerScript _player;
        private float _mag;
        private float _shakeTime;
        public GameObject _bossbar;
    }
}