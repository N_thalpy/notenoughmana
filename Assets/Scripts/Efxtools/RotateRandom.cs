﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRandom : MonoBehaviour {

    // Update is called once per frame
    private float _rate;
    public float Max;
    public float Min;
    void Start()
    {
        _rate = Random.Range(Max,Min);
        
    }
	void Update () {
        gameObject.transform.Rotate(0f, 0f, _rate* Time.deltaTime);
	}
}
