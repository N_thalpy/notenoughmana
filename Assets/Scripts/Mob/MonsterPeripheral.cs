﻿using NotEnoughMana.QuickCollision;
using UnityEngine;
using Random = System.Random;

namespace NotEnoughMana.Mob
{
    public class MonsterPeripheral : MonsterScriptBase
    {
        /*
        public int MaxHP;
        public int CurrentHP;
        public int Defense;
        public int ManaOrbCount;
        */

        public int CollateralDamage;        

        public MonsterScriptBase Core;

        protected override void Start()
        {
            base.Start();
            CurrentHP = MaxHP;

            Layer = QuickColliderLayer.DoodadOrEnemy;
        }
        protected override void Update()
        {
            if (InitialCooldown > 0)
                InitialCooldown -= Time.deltaTime;
            if (CurrentHP <= 0)
            {
                GameObject.Destroy(this.gameObject);
                Core.CurrentHP -= CollateralDamage;
                Random rd = new Random();
                GameObject manaOrb = Resources.Load<GameObject>("manaorb");
                for (int idx = 0; idx < ManaOrbCount; idx++)
                    GameObject.Instantiate(manaOrb, transform.position + new Vector3(rd.NextFloat() - 0.5f, rd.NextFloat() - 0.5f, 0), Quaternion.identity);
            }
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());
            

            ManaOrbCount = 4;
        }
        public override void OnCollision(QuickColliderBase opp)
        {
            base.OnCollision(opp);
        }

    }
}