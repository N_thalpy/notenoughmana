﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;
using NotEnoughMana.Map;

using UnityEngine;
using System;

using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class FriendAI : MonsterScriptBase
    {
        public GameObject PopupEffect;

        private float timer;

        float flowSpeed { get { return MapGenerator.Instance.FlowSpeed; } }
        
        private float cooldownTimer;
        protected override void Start()
        {
            MaxHP = 10000000;
            base.Start();
            timer = 0;
            Layer = QuickColliderLayer.PlayerUnit;
        }

        public Vector3 Initpos;

        public override void Initialize()
        {
            transform.position = Initpos;

            if (PopupEffect != null)
                GameObject.Instantiate(PopupEffect, transform.position, Quaternion.identity);

            Layer = QuickColliderLayer.PlayerUnit;
        }

        void Move()
        {
            gameObject.transform.position += new Vector3(0, -flowSpeed * Time.deltaTime, 0);
        }

        protected override void Update()
        {
            base.Update();

            Move();

            if (gameObject.transform.position.y < -4.5) {
                GameObject.Destroy(this.gameObject);
            }           


            if (InitialCooldown > 0)
                return;
            
            timer += Time.deltaTime;

            cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);

            GameObject prefab = Resources.Load<GameObject>("basic_long_f1");
            Debug.Assert(prefab != null, "Prefab 'basic_long_f1', for FriendAI, has not found");
            if (cooldownTimer == 0)
            {
                cooldownTimer += 1.5f;
                Vector2 align = transform.position.XY();

                Vector2 dir = Vector3.up;

                float angle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                GameObject cloned = GameObject.Instantiate(prefab, transform.position, Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg));
                cloned.GetComponent<QuickCircleCollider>().Layer = QuickColliderLayer.PlayerProjectile;
                cloned.GetComponent<QuickCircleCollider>().SkipList.Add(this.GetComponent<QuickCircleCollider>());
                //cloned.GetComponent<LinearMove>().Velocity = dir.normalized * (5f - Math.Abs(3.5f - idx) * 0.75f);
                cloned.GetComponent<LinearMove>().Velocity = dir.normalized * 2.0f;
                cloned.GetComponent<Lifetime>().SetLifetime(10f);


            }

        }
    }
}
