﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;
using NotEnoughMana.Map;

using UnityEngine;
using System;

using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class TurretAI : MonsterScriptBase
    {
        public GameObject PopupEffect;

        private float timer;

        float flowSpeed { get { return MapGenerator.Instance.FlowSpeed; } }
        
        private float cooldownTimer;
        protected override void Start()
        {
            base.Start();
            timer = 0;
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());
            transform.position = new Vector3(4 * (rd.NextFloat() - 0.5f), 4 + rd.NextFloat() * 1, 0);

            if (PopupEffect != null)
                GameObject.Instantiate(PopupEffect, transform.position, Quaternion.identity);
        }

        void Move()
        {
            gameObject.transform.position += new Vector3(0, -flowSpeed * Time.deltaTime, 0);
        }

        protected override void Update()
        {
            base.Update();

            if (gameObject.transform.position.y < -4.5) {
                GameObject.Destroy(this.gameObject);
            }

            Move();

            if (InitialCooldown > 0)
                return;


            timer += Time.deltaTime;

            cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);

            if (!CanShot) {
                return;
            }

            GameObject prefab = Resources.Load<GameObject>("basic_big_ice");
            Debug.Assert(prefab != null, "Prefab 'withtrail', for SimpleAI, has not found");
            if (cooldownTimer == 0)
            {
                cooldownTimer += 1f;
                Vector2 align = PlayerScript.GetPlayerPosition().XY() - transform.position.XY();
                for (int idx = 0; idx < 8; idx++)
                {
                    float deltaAngle = idx*45f;                    
                    
                    Vector2 dir = (Quaternion.Euler(0, 0, deltaAngle) * new Vector3(1,0,0)).XY();

                    float angle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                    GameObject cloned = GameObject.Instantiate(prefab, transform.position, Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg));
                    cloned.GetComponent<QuickCircleCollider>().Layer = QuickColliderLayer.EnemyProjectile;
                    cloned.GetComponent<QuickCircleCollider>().SkipList.Add(this.GetComponent<QuickCircleCollider>());
                    //cloned.GetComponent<LinearMove>().Velocity = dir.normalized * (5f - Math.Abs(3.5f - idx) * 0.75f);
                    cloned.GetComponent<LinearMove>().Velocity = dir.normalized *1.2f;
                    cloned.GetComponent<Lifetime>().SetLifetime(5f);

                }
            }

        }
    }
}
