﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;
using NotEnoughMana.Map;

using UnityEngine;
using System;

using Random = System.Random;
using NotEnoughMana.QuickCollision.Impl;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class ManaBombAI : MonsterScriptBase
    {
        //public float InitSpeed = 10.0f;
        public float ExplosionCooltime = 3.0f;
        public float ExplosionRadius = 2.0f;
        public float ExplosionEffecttime = 0.5f;
        private float currentRadius;
        private float timer;
        private bool isExplode;
        private GameObject explodeEffect;
        //private float speed;
        
        protected override void Start()
        {
            base.Start();
            currentRadius = 0.0f;
            isExplode = false;
            //speed = InitSpeed;
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());
            
            transform.position = new Vector3(12 * (rd.NextFloat() - 0.5f), 4 + 2 * rd.NextFloat(), 0);
        }

        protected override void Update()
        {
            base.Update();
            timer += Time.deltaTime;
            if (isExplode)
            {
                if (timer >= ExplosionEffecttime)
                {
                    timer = ExplosionEffecttime;
                }

                currentRadius = ExplosionRadius * timer / ExplosionRadius;
                explodeEffect.transform.localScale = Vector3.one * currentRadius;
                foreach (ManaOrb orb in GameObject.FindObjectsOfType<ManaOrb>())
                {
                    if ((orb.transform.position - transform.position).magnitude < currentRadius)
                    {
                        GameObject.Destroy(orb.gameObject);
                    }
                }

                if(timer == ExplosionEffecttime)
                {
                    GameObject.Destroy(this.gameObject);
                }
            }
            else
            {
                transform.position += Vector3.down * Time.deltaTime * MapGenerator.Instance.FlowSpeed * 2.0f;
                if (timer >= ExplosionCooltime)
                {
                    Explode();
                }
            }
        }

        private void StartExplode()
        {
            Debug.Log("BOOM!");
            explodeEffect = new GameObject("ExplodeEffect");
            SpriteRenderer renderer = explodeEffect.AddComponent<SpriteRenderer>();
            renderer.sprite = Resources.Load<Sprite>("CIRCLE_2");
            explodeEffect.transform.localScale = Vector3.zero;
            ManaOrbCount = 0;
            timer = 0.0f;
            isExplode = true;
        }

        private void Explode()
        {
            Debug.Log("KABOOM!");
            explodeEffect = GameObject.Instantiate(Resources.Load<GameObject>("exp_ice"), transform.position, Quaternion.identity);
            explodeEffect.transform.position = transform.position;
            ManaOrbCount = 0;
            GameObject player = PlayerScript.Instance;
            if((player.transform.position - transform.position).magnitude < ExplosionRadius)
            {
//                player.GetComponent<PlayerScript>().Mana 
            }

            foreach (ManaOrb orb in GameObject.FindObjectsOfType<ManaOrb>())
            {
                Debug.Log((orb.transform.position - transform.position).magnitude);
                if ((orb.transform.position - transform.position).magnitude < ExplosionRadius)
                {
                    GameObject.Destroy(orb.gameObject);
                }
            }

            GameObject.Destroy(this.gameObject);
        }

        public override void OnCollision(QuickColliderBase opp)
        {
            base.OnCollision(opp);
            if (opp is PlayerHitCollider && !isExplode)
            {
                StartExplode();
            }
        }

        protected override void Awake()
        {
            base.Awake();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        protected override void OnDrawGizmosSelected()
        {
            base.OnDrawGizmosSelected();
        }
    }
}
