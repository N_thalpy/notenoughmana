﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;

using UnityEngine;
using System;
using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class LinkDroneAI : MonsterScriptBase
    {
        public enum MoveState
        {
            Wait,
            Start,
            Arrive,
            Move,
            END
        }

        MoveState _state = MoveState.Wait;
        public MoveState State { get { return _state; } }
        public LinkDroneAI Front;
        public LinkDroneAI Back;

        [Range(1, 5)]
        public int Length = 1;

        int dir = 0;
        public float Speed = 1.5f;

        float width = 7.0f;

        private Vector3 targetPos;
        Vector3 prevPos;
        private float timer;
        public bool leading = false;

        float deathDoorTimer = -100f;
        bool deathDoor = false;

        private float cooldownTimer;
        protected override void Start()
        {
            base.Start();
            timer = 0;

            if (Length > 1) {
                GameObject prefab = Resources.Load<GameObject>("mob/LinkDroneFollower");

                //if (leading) {
                //    prefab = Resources.Load<GameObject>("mob/LinkDroneLeader");
                //} else {
                //    prefab = Resources.Load<GameObject>("mob/LinkDroneFollower");
                //}

                var newOne = GameObject.Instantiate(prefab);
                var newAI = newOne.GetComponent<LinkDroneAI>();
                newAI.Length = this.Length - 1;
                Back = newAI;
                newAI.Front = this;
                newAI.Initialize();
                newAI.leading = false;
            }
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());

            _state = MoveState.Wait;
            dir = rd.Next(0, 2) == 0 ? -1 : 1;
            if (Front != null) {
                dir = Front.dir;
            }
            var h = 3f + rd.NextFloat();
            if(Front != null) {
                h = Front.transform.position.y;
            }
            transform.position = new Vector3(-width * dir, h, 0);
            targetPos = new Vector3(width * dir, 4, 0);
            prevPos = transform.position;

            InitialCooldown = 1.0f;
        }

        void Move()
        {
            var del = Time.deltaTime;

            switch (_state) {
                case MoveState.Start:
                    transform.position += Speed * dir * Vector3.right * del;
                    if ((prevPos.x - transform.position.x) * -dir > 1f) {
                        _state = MoveState.Move;
                    }
                    break;
                case MoveState.Move:
                    transform.position += Speed * dir * Vector3.right * del;

                    if ((targetPos.x - transform.position.x) * dir < 0) {
                        _state = MoveState.Arrive;
                    }
                    break;
                case MoveState.Wait:
                    if (Front == null) {
                        StartMove();
                    } else if (Front.State == MoveState.Move) {
                        StartMove();
                    }
                    break;
                case MoveState.Arrive:
                    if (Back == null) {
                        Random rd = new Random(this.GetHashCode());
                        var vec = rd.NextFloat() * 0.5f + 1;
                        ReadyMove(vec);
                    }
                    break;
                case MoveState.END:
                    Destroy(this.gameObject);
                    break;
            }
        }
        
        void StartMove()
        {
            if (transform.position.y > 0) {
                _state = MoveState.Start;
            } else {
                _state = MoveState.END;
            }
        }

        void ReadyMove(float vec)
        {
            _state = MoveState.Wait;
            if (transform.position.x < 0) {
                dir = 1;
            } else {
                dir = -1;
            }
            transform.position -= new Vector3(0, vec, 0);
            prevPos = targetPos;
            targetPos = new Vector3(dir * width, transform.position.y, 0);

            if (Front != null) {
                Front.ReadyMove(vec);
            }
        }

        protected override void Update()
        {
            base.Update();
            Move();

            timer += Time.deltaTime;

            cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);

            if(deathDoorTimer > 0) {
                deathDoorTimer -= Time.deltaTime;
                if(deathDoorTimer <= 0) {
                    ProcessDestroy();
                    Destroy(this.gameObject);
                    return;
                }
            }

            if (!CanShot) {
                return;
            }

            if (_state == MoveState.Start || _state == MoveState.Move) {
                if (InitialCooldown > 0)
                    return;

                GameObject prefab = Resources.Load<GameObject>("withtrail");
                Debug.Assert(prefab != null, "Prefab 'withtrail', for SimpleAI, has not found");
                if (cooldownTimer == 0) {
                    cooldownTimer += 1.5f;

                    Vector2 dir = PlayerScript.GetPlayerPosition().XY() - transform.position.XY();
                    //Vector2 dir = (Quaternion.Euler(0, 0, deltaAngle) * align.XY0()).XY();

                    float angle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                    GameObject cloned = GameObject.Instantiate(prefab, transform.position, Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg));
                    cloned.GetComponent<QuickCircleCollider>().Layer = QuickColliderLayer.EnemyProjectile;
                    cloned.GetComponent<LinearMove>().Velocity = dir.normalized * 2f;
                    cloned.GetComponent<Lifetime>().SetLifetime(5f);
                }
            }
        }

        protected override void OnDestroy()
        {
            if (leading || deathDoor) {
                if (Back != null) {
                    Back.EnterDeathDoor();
                }
            }

            if (Back != null) {
                Back.Front = this.Front;
            }
            if (Front != null) {
                Front.Back = this.Back;
            }

            base.OnDestroy();
        }

        void EnterDeathDoor()
        {
            deathDoor = true;
            deathDoorTimer = 0.15f;
        }
    }
}
