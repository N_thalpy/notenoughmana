﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;
using NotEnoughMana.UI;

using UnityEngine;
using System;
using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class ShieldAI : MonsterPeripheral
    {
        private float timer;
        private float cooldownTimer;

        protected override void Start()
        {
            base.Start();
            timer = 0;
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());
        }

        protected override void Update()
        {
            base.Update();
            if (InitialCooldown > 0)
                return;

            timer += Time.deltaTime;            
            cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);
            

        }
        private void OnHit()
        {
            GameObject prefab = Resources.Load<GameObject>("basic_long_f1");
            Debug.Assert(prefab != null, "Prefab 'withtrail', for SimpleAI, has not found");
            if (cooldownTimer == 0)
            {
                //UIManager.Instance.ShakeCamera(1f, 0.3f);
                cooldownTimer += 1f;
                Core.Knockback(0.3f);
                //Vector2 dir = (Quaternion.Euler(0, 0, deltaAngle) * align.XY0()).XY();                
                for (int idx = 0; idx < 8; idx++)
                {
                    float deltaAngle = (idx - 7f / 2) * 4;
                    Vector2 dir = PlayerScript.GetPlayerPosition().XY() - transform.position.XY();

                    float angle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                    GameObject cloned = GameObject.Instantiate(prefab, transform.position + new Vector3(0.4375f, 0f, 0f) - new Vector3(0.125f * idx + 0f, 0f), Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg));
                    cloned.GetComponent<QuickCircleCollider>().Layer = QuickColliderLayer.EnemyProjectile;
                    cloned.GetComponent<QuickCircleCollider>().SkipList.Add(this.GetComponent<QuickCircleCollider>());
                    cloned.GetComponent<LinearMove>().Velocity = dir.normalized * (4f - Math.Abs(3.5f - idx) * 0.75f);
                    cloned.GetComponent<Lifetime>().SetLifetime(5f);

                }
            }
        }
        public override void OnCollision(QuickColliderBase opp)
        {            switch (opp.Layer)
            {
                case QuickColliderLayer.PlayerProjectile:
                    OnHit();
                    break;
                default:
                    break;
            }
        }
    }
}
