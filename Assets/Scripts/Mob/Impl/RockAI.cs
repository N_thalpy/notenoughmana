﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;
using NotEnoughMana.Map;

using UnityEngine;
using System;

using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class RockAI : MonsterScriptBase
    {

        private float timer;

        public float FlowSpeed = 10.0f;

        private float cooldownTimer;
        protected override void Start()
        {
            base.Start();
            timer = 0;
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());
            transform.position = new Vector3(5.5f * (rd.NextFloat() - 0.5f), 6 + rd.NextFloat() * 4.0f, 0);

            InitialCooldown = rd.NextFloat() * 2.0f;
        }

        float speedAngle = 0f;
        int dir = 1;
        public float minSpeed = 2.0f;

        void Move()
        {
            var mid = (minSpeed + FlowSpeed) / 2;
            speedAngle += 3f;

            var cSpeed = minSpeed + Mathf.Abs((FlowSpeed - minSpeed) * Mathf.Cos(speedAngle * Mathf.Deg2Rad));
            
            gameObject.transform.position += new Vector3(0, -cSpeed * Time.deltaTime, 0);
        }

        protected override void Update()
        {
            base.Update();

            if (gameObject.transform.position.y < -4.5) {
                GameObject.Destroy(this.gameObject);
            }

            if (InitialCooldown > 0)
                return;
            Move();

            timer += Time.deltaTime;
        }
    }
}
