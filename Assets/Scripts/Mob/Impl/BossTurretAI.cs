﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.QuickCollision.Impl;
using NotEnoughMana.Control;
using NotEnoughMana.Map;

using UnityEngine;
using System;

using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class BossTurretAI : MonsterPeripheral
    {
        private enum ShotState
        {
            Wait,
            Ready,
            Shot,
            Regen,
        }

        private enum TurretType
        {
            Spread,
            Beam,
            EOE
        }

        private ShotState state = ShotState.Wait;

        [Range(5, 50)]
        public int Magazine = 40;
        [Range(0.05f, 0.5f)]
        public float ShotTime = 0.05f;
        [Range(15f, 180f)]
        public float ShotAngle = 20f;
        [Range(1.0f, 7.0f)]
        public float ShotSpeed = 2.6f;

        private float timer;
        private float coolDown = 2.5f;

        private float flowSpeed { get { return MapGenerator.Instance.FlowSpeed; } }

        private float cooldownTimer;

        private float shotTimer = 0f;
        private int shotCount = 0;

        private float _regen = 20f;
        private float _regenTimer;

        private TurretType _type;
                
        protected override void Start()
        {
            base.Start();
            timer = 0;
            Random rd = new Random(this.GetHashCode());

            _type = (TurretType)rd.Next(0, 1);
            Debug.Log(_type);

            InitialCooldown = 2.0f;
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());

            _type  = (TurretType)rd.Next(0, 1);
            Debug.Log(_type);

            InitialCooldown = 2.0f;
            ManaOrbCount = 12;
        }

        protected override bool CanShot
        {
            get
            {
                return transform.position.y < 4;
            }
        }

        protected override void Update()
        {
            //base.Update();
            if (InitialCooldown > 0)
                InitialCooldown -= Time.deltaTime;

            if(state == ShotState.Regen)
            {
                _regenTimer -= Time.deltaTime;
                if(_regenTimer<0)
                {
                    state = ShotState.Wait;
                    CurrentHP = 100;

                    this.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
                    gameObject.GetComponent<MonsterPeripheral>().Active = true;
                }
                return;
            }
            if (CurrentHP <= 0)
            {
                _regenTimer = _regen;
                state = ShotState.Regen;
                (Core as BossCannonAI).OnTurretDestroyed();
                //GameObject.Destroy(this.gameObject);
                this.gameObject.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0f);
               gameObject.GetComponent<MonsterPeripheral>().Active = false;

                Core.CurrentHP -= CollateralDamage;
                Random rd = new Random();
                GameObject manaOrb = Resources.Load<GameObject>("manaorb");
                for (int idx = 0; idx < ManaOrbCount; idx++)
                    GameObject.Instantiate(manaOrb, transform.position + new Vector3(rd.NextFloat() - 1.5f, rd.NextFloat() - 1.5f, 0), Quaternion.identity);
            }
            
            Vector2 targetPos = PlayerScript.Instance.transform.position;
            float theta = Mathf.Atan2(targetPos.y - transform.position.y, targetPos.x - transform.position.x);
            transform.rotation = Quaternion.Euler(0, 0, theta * Mathf.Rad2Deg - 90);

            if (gameObject.transform.position.y < -4.5)
                GameObject.Destroy(this.gameObject);
            
            if (InitialCooldown > 0)
                return;

            var delta = Time.deltaTime;
            timer += delta;

            if (!CanShot) {
                return;
            }

            cooldownTimer = Math.Max(cooldownTimer - delta, 0);

            if (cooldownTimer == 0)
            {
                switch (state)
                {
                    case ShotState.Wait:
                        state = ShotState.Ready;
                        cooldownTimer = 0.5f;
                        break;

                    case ShotState.Ready:
                        state = ShotState.Shot;
                        break;

                    case ShotState.Shot:
                        switch (_type)
                        {
                            case TurretType.Spread:
                                if (Gunshot(timer))
                                {
                                    state = ShotState.Wait;
                                    cooldownTimer = coolDown;
                                }
                                break;
                            case TurretType.Beam:
                               
                                break;
                        }
                        break;
                }
            }
        }

        private bool Gunshot(float time)
        {
            var delta = time - shotTimer;

            if (delta >= ShotTime)
            {
                shotTimer = time;
                int evenCount = (Magazine - 1) / 2;
                int realCount = shotCount;
                if (shotCount > evenCount)
                    realCount = Magazine - shotCount - 1;

                var angle = realCount * (ShotAngle / evenCount) - (ShotAngle / 2);

                var _align = RotateVector(PlayerScript.GetPlayerPosition().XY() - transform.position.XY(), angle);

                GameObject prefab = Resources.Load<GameObject>("basic_big_ice");
                Debug.Assert(prefab != null, "Prefab 'withtrail', for SimpleAI, has not found");
                var shotAngle = angle + Vector2.Angle(PlayerScript.GetPlayerPosition().XY(), transform.position.XY());

                GameObject cloned = GameObject.Instantiate(prefab, transform.position + new Vector3(0f,-0.65f,0f), Quaternion.Euler(0, 0, shotAngle * Mathf.Rad2Deg));
                cloned.GetComponent<QuickCircleCollider>().Layer = QuickColliderLayer.EnemyProjectile;
                cloned.GetComponent<QuickCircleCollider>().SkipList.Add(this.GetComponent<QuickCircleCollider>());
                //cloned.GetComponent<LinearMove>().Velocity = transform.rotation * Vector3.up * ShotSpeed;
                cloned.GetComponent<LinearMove>().Velocity = _align.normalized * ShotSpeed;
                cloned.GetComponent<Lifetime>().SetLifetime(5f);

                shotCount++;
            }

            if (shotCount == Magazine)
            {
                shotCount = 0;
                return true;
            }
            return false;
        }
        Vector2 RotateVector(Vector2 vec, float _angle)
        {
            var angle = Mathf.Deg2Rad * _angle;
            var result = new Vector2(vec.x * Mathf.Cos(angle) - vec.y * Mathf.Sin(angle),
                vec.y * Mathf.Cos(angle) + vec.x * Mathf.Sin(angle));
            return result;
        }
    }
}
