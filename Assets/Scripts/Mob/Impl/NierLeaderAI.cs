﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;

using UnityEngine;
using System;
using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class NierLeaderAI : MonsterScriptBase
    {
        public enum MoveState
        {
            Wait,
            Start,
            Arrive,
            Move,
            END
        }

        MoveState _state = MoveState.Wait;
        public NierFollowerAI[] Back;
        public int childLen { get { return Back.Length; } }

        [Range(2, 4)]
        public int Length = 1;
        
        public float Speed = 1f;

        float width = 7.0f;
        float fieldWidth = 5.0f;

        private Vector3 targetPos;
        Vector3 prevPos;
        private float timer;
        
        float _moveAngle = 0f;
        public float MoveAngle { get { return _moveAngle; } }

        private float cooldownTimer;

        Random rd;

        protected override void Start()
        {
            base.Start();
            timer = 0;
            
            GameObject prefab = Resources.Load<GameObject>("mob/NierFollower");
            Back = new NierFollowerAI[Length];

            for (int i = 0; i < Length; i++) {
                var newOne = GameObject.Instantiate(prefab);
                var newAI = newOne.GetComponent<NierFollowerAI>();
                newAI.Order = i;
                Back[i] = newAI;
                newAI.Leader = this;
                newAI.Initialize();
            }

        }

        public override void Initialize()
        {
            rd = new Random(this.GetHashCode());

            var dir = rd.Next(0, 2) == 0 ? -1 : 1;

            transform.position = new Vector3(width * dir, 9f, 0);

            var h = 2.5f + rd.NextFloat() * 0.7f;
            targetPos = new Vector3(fieldWidth * dir, h, 0);
            _state = MoveState.Start;

            InitialCooldown = 2.0f;
        }

        void Move()
        {
            var cSpeed = Speed;
            if(_state == MoveState.Start) {
                cSpeed = Speed * 3f;
            }

            switch (_state) {
                case MoveState.Start:
                case MoveState.Move:
                    var dir = targetPos - transform.position;
                    if (dir.magnitude > cSpeed) {
                        dir = dir.normalized * cSpeed;
                    } else if (dir.magnitude < cSpeed / 3) {
                        dir = dir.normalized * cSpeed / 3;
                    }

                    transform.position += dir * Time.deltaTime;

                    if ((transform.position - targetPos).magnitude < 0.1f) {
                        _state = MoveState.Arrive;
                    }
                    break;
                case MoveState.Arrive:
                    var n = rd.Next(0, 2) == 0 ? -1 : 1;
                    var h = 1f + rd.NextFloat() * 2.3f;

                    var nV = new Vector3(fieldWidth * n * rd.NextFloat(), h, 0);
                    var len = nV - transform.position;
                    if (len.magnitude > 5f) {
                        len = len.normalized * 5f;
                    } else if(len.magnitude < 2f) {
                        len = len.normalized * 2f;
                    }
                    targetPos = transform.position + len;
                    
                    _state = MoveState.Move;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
        
        protected override void Update()
        {
            base.Update();
            Move();

            timer += Time.deltaTime;

            _moveAngle = timer * 60f;
        }

        protected override void OnDestroy()
        {
           for(int i = 0; i < Back.Length; i++) {
                var tmp = Back[i];
                if(tmp == null) {
                    continue;
                }

                tmp.EnterDeathdoor();
            }

            base.OnDestroy();
        }
        
    }
}
