﻿using NotEnoughMana.Control;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Map;
using NotEnoughMana.Mob;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.QuickCollision.Impl;
using System;
using UnityEngine;
using NotEnoughMana.UI;
using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class BossCannonAI : MonsterScriptBase
    {
        public AudioClip LazerSound;
        public AudioClip LongRazerSound;

        private enum BossState
        {
            Wait,
            Ready,
            Shot,
            Stunned,
            InActive
        }

        public float MaxWaitRotationSpeed;
        public float MaxShotRotationSpeed;

        private GameObject beamPrefab;
        private GameObject beam;

        BossState state = BossState.InActive;

        private float flowSpeed
        {
            get
            {
                return MapGenerator.Instance.FlowSpeed;
            }
        }

        public bool BossEncountered = false;

        private float cooldown = 8f;
        private float cooldownTimer;

        private float channeling = 3f;
        private float channelingTimer;
        private float shotDuration = 35f;
        private float shotDurationTimer;

        private float stunDuration = 8f;
        private float stunTimer;
        private EnergyBar hpbar;
        public void OnTurretDestroyed()
        {
            gameObject.GetComponentInChildren<ParticleSystem>().Stop(true);
            GameObject.Destroy(beam);
            beam = null;
            state = BossState.Stunned;
            stunTimer = stunDuration;
            Defense = 0;
            gameObject.GetComponent<SpriteRenderer>().color = new Color(0.4f, 0.4f, 0.4f);
        }
        public override void OnCollision(QuickColliderBase opp)
        {
            switch (opp.Layer)
            {
                case QuickColliderLayer.PlayerProjectile:
                    if (state == BossState.InActive && BossEncountered == true)
                    {
                        UIManager.Instance._bossbar.SetActive(true);
                        hpbar = UIManager.Instance._bossbar.GetComponent<EnergyBar>();
                        hpbar.valueCurrent = CurrentHP;
                        state = BossState.Wait;
                    }
                    break;

                default:
                    break;
            }
        }

        protected override void Start()
        {
            base.Start();
            beamPrefab = Resources.Load<GameObject>("CannonBeam");
            hpbar = new EnergyBar();
            shotDurationTimer = 0;
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());

            InitialCooldown = 2.0f;

            cooldownTimer = InitialCooldown;
        }

        protected override bool CanShot
        {
            get
            {
                return transform.position.y <= 4;
            }
        }

        Random rd = new Random();

        protected override void Update()
        {
            base.Update();
            hpbar.valueCurrent = CurrentHP;
            #region Target Tracking
            Vector2 targetPos = PlayerScript.Instance.transform.position;
            float targetTheta = Mathf.Atan2(targetPos.y - transform.position.y, targetPos.x - transform.position.x) * Mathf.Rad2Deg + 90;
            float currentTheta = transform.rotation.eulerAngles.z;

            float pos = Mathf.Abs(720 + currentTheta - targetTheta);
            float neg = Mathf.Abs(720 + targetTheta - currentTheta);

            while (neg > 360)
                neg -= 360;
            while (pos > 360)
                pos -= 360;

            float sign = pos < neg ? 1 : -1;

            switch (state)
            {
                case BossState.Wait:

                    if (Math.Abs(targetTheta - currentTheta) < MaxWaitRotationSpeed * Time.deltaTime)
                        currentTheta = targetTheta;
                    else
                        currentTheta += sign * MaxWaitRotationSpeed * Time.deltaTime;

                    break;
                case BossState.Ready:
                    break;
                case BossState.Shot:
                    if (Math.Abs(targetTheta - currentTheta) < MaxShotRotationSpeed * Time.deltaTime)
                        currentTheta = targetTheta;
                    else
                        currentTheta += sign * MaxShotRotationSpeed * Time.deltaTime;

                    break;
            }

            transform.rotation = Quaternion.Euler(0, 0, currentTheta);
            #endregion

            if (gameObject.transform.position.y < -4.5)
                GameObject.Destroy(this.gameObject);

            if (beam != null)
            {
            }
            
            if (!CanShot) {
                return;
            }

            if (rd.NextFloat() < 0.02f) {
                var prefab = Resources.Load<GameObject>("manaorb");
                var mn = GameObject.Instantiate(prefab, new Vector3(4 * (rd.NextFloat() - 0.5f), 2 + rd.NextFloat(), 0), Quaternion.identity);
            }

            switch (state)
            {
                case BossState.Wait:
                    cooldownTimer -= Time.deltaTime;
                    if (cooldownTimer < 0)
                    {
                        channelingTimer = channeling;
                        state = BossState.Ready;
                    }
                    break;

                case BossState.Ready:
                    channelingTimer -= Time.deltaTime;
                    if (channelingTimer < 0)                    
                        state = BossState.Shot;
                    shotDurationTimer = shotDuration;
                    MaxShotRotationSpeed = 5f;
                    gameObject.transform.Find("laser_small").GetComponentInChildren<ParticleSystem>().Play(true);
                    break;

                case BossState.Shot:
                    GameObject.Find("BackGroundMusic").GetComponent<AudioSource>().PlayOneShot(LazerSound);

                    shotDurationTimer -= Time.deltaTime;
                    if (beam == null)
                    {
                        beam = GameObject.Instantiate(beamPrefab, transform.position, Quaternion.identity);
                        beam.GetComponent<BeamCollider>().Layer = QuickColliderLayer.EnemyProjectile;
                        gameObject.transform.Find("laser_small").GetComponentInChildren<ParticleSystem>().Stop(true);
                        gameObject.transform.Find("laser_new 2").GetComponentInChildren<ParticleSystem>().Play(true);
                        //gameObject.GetComponentInChildren<ParticleSystem>().Play(true);

                        UIManager.Instance.ShakeCamera(1f, 0.3f);
                    }
                    MaxShotRotationSpeed +=0.03f;
                    beam.transform.position = this.transform.position;
                    beam.transform.rotation = this.transform.rotation;

                    if (shotDurationTimer < 0)
                    {
                        cooldownTimer = cooldown;
                        state = BossState.Wait;
                        gameObject.transform.Find("laser_new 2").GetComponentInChildren<ParticleSystem>().Stop(true);
                        //gameObject.GetComponentInChildren<ParticleSystem>().Stop(true);
                        GameObject.Destroy(beam);
                        beam = null;
                    }
                    break;

                case BossState.Stunned:
                    {
                        stunTimer -= Time.deltaTime;
                        if(stunTimer<0)
                        {
                            gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f);
                            Defense = 999;
                            cooldownTimer = cooldown;
                            state = BossState.Wait;
                        }
                        break;
                    }
            }
        }
    }
}
