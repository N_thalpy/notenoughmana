﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;

using UnityEngine;
using System;
using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class NierFollowerAI : MonsterScriptBase
    {
        public NierLeaderAI Leader;

        Vector3 _leadPos;
        Vector3 leadPos
        {
            get
            {
                if(Leader != null) {
                    _leadPos = Leader.transform.position;
                }
                if (_leadPos != null) {
                    return _leadPos;
                } else {
                    return transform.position;
                }
            }
        }
                
        [NonSerialized]
        public int Order = 0;

        private Vector3 targetPos;
        Vector3 prevPos;
        private float timer;

        float deathDoorTimer = -100f;
        bool deathDoor = false;

        private float cooldownTimer;
        protected override void Start()
        {
            base.Start();
            timer = 0;

            deathDoor = false;
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());

            transform.position = Leader.transform.position;
            prevPos = transform.position;            

            InitialCooldown = 1.0f;
        }

        float _angle;

        void Move()
        {
            var angle = Leader.MoveAngle;
            var del = 360f / (float)Leader.childLen * Order;
            angle += del;

            var len = Vector3.right;
            len = RotateVector(len, angle);
            transform.position = leadPos + len;

            _angle = angle - 90;

            var q = Quaternion.Euler(0, 0, _angle);
            transform.rotation = q;
        }
        
        protected override void Update()
        {
            base.Update();
            Move();

            timer += Time.deltaTime;

            cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);
            
            if (deathDoorTimer > 0) {
                deathDoorTimer -= Time.deltaTime;
                if (deathDoorTimer <= 0) {
                    ProcessDestroy();
                    Destroy(this.gameObject);
                    return;
                }
            }
            
            if (InitialCooldown > 0)
                return;

            if (!CanShot) {
                return;
            }

            GameObject prefab = Resources.Load<GameObject>("basic_f");
            Debug.Assert(prefab != null, "Prefab 'basic_f', for NierFollowerAI, has not found");
            if (cooldownTimer == 0) {
                cooldownTimer += 0.4f;

                float angle = _angle + 90;
                Vector2 dir = RotateVector(Vector3.right, angle);                

                GameObject cloned = GameObject.Instantiate(prefab, transform.position, Quaternion.Euler(0, 0, angle-90));
                cloned.GetComponent<QuickCircleCollider>().Layer = QuickColliderLayer.EnemyProjectile;
                cloned.GetComponent<LinearMove>().Velocity = dir.normalized * 4f;
                cloned.GetComponent<Lifetime>().SetLifetime(5f);
            }
        }
        
        public void EnterDeathdoor()
        {
            deathDoor = true;
            deathDoorTimer = 0.3f;
        }

        Vector2 RotateVector(Vector2 vec, float _angle)
        {
            var angle = Mathf.Deg2Rad * _angle;
            var result = new Vector2(vec.x * Mathf.Cos(angle) - vec.y * Mathf.Sin(angle),
                vec.y * Mathf.Cos(angle) + vec.x * Mathf.Sin(angle));
            return result;
        }
    }
}
