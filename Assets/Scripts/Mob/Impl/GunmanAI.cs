﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;
using NotEnoughMana.Map;

using UnityEngine;
using System;

using Random = System.Random;
using NotEnoughMana.Assets.Scripts.Movement;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class GunmanAI : MonsterScriptBase
    {
        enum ShotState
        {
            Wait,
            Ready,
            Shot
        }

        ShotState _state = ShotState.Wait;

        [Range(5, 36)]
        public int Magazine = 12;
        [Range(0.05f, 0.5f)]
        public float ShotTime = 0.3f;
        [Range(15f, 360)]
        public float ShotAngle = 20f;
        [Range(1.0f, 7.0f)]
        public float ShotSpeed = 3.6f;

        private float timer;

        float flowSpeed { get { return MapGenerator.Instance.FlowSpeed; } }

        private float cooldownTimer;
        protected override void Start()
        {
            base.Start();
            timer = 0;
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());
            transform.position = new Vector3(4 * (rd.NextFloat() - 0.5f), 4 + rd.NextFloat() * 2, 0);

            InitialCooldown = 0.5f;

            ManaOrbCount = 4;
        }

        void Move()
        {
            gameObject.transform.position += new Vector3(0, -flowSpeed * Time.deltaTime, 0);
        }

        float coolDown = 2f;

        protected override void Update()
        {
            base.Update();

            if (gameObject.transform.position.y < -4.5)
            {
                GameObject.Destroy(this.gameObject);
            }

            Move();

            if (InitialCooldown > 0)
                return;

            var delta = Time.deltaTime;
            timer += delta;

            cooldownTimer = Math.Max(cooldownTimer - delta, 0);

            if (!CanShot) {
                return;
            }

            if (cooldownTimer == 0)
            {
                switch (_state)
                {
                    case ShotState.Wait:
                        _state = ShotState.Ready;
                        cooldownTimer = 0.5f;
                        GunReady();
                        break;

                    case ShotState.Ready:
                        _state = ShotState.Shot;
                        break;

                    case ShotState.Shot:
                        if (Gunshot(timer))
                        {
                            _state = ShotState.Wait;
                            cooldownTimer = coolDown;
                        }
                        break;
                }
            }
        }

        Vector2 shotAlign;

        void GunReady()
        {
            shotAlign = PlayerScript.GetPlayerPosition().XY() - transform.position.XY();
        }

        float shotTimer = 0f;
        int shotCount = 0;

        bool Gunshot(float time)
        {
            var delta = time - shotTimer;

            if (delta >= ShotTime)
            {
                shotTimer = time;
                int evenCount = (Magazine - 1) / 2;
                int realCount = shotCount;
                if (shotCount > evenCount)
                {
                    realCount = Magazine - shotCount - 1;
                }
                var angle = realCount * (ShotAngle / evenCount) - (ShotAngle / 2);
                var _align = RotateVector(shotAlign, angle);

                GameObject prefab = Resources.Load<GameObject>("lava_big");
                Debug.Assert(prefab != null, "Prefab 'withtrail', for SimpleAI, has not found");
                var shotAngle = angle + Vector2.Angle(PlayerScript.GetPlayerPosition().XY(), transform.position.XY());


                GameObject cloned = GameObject.Instantiate(prefab, transform.position + new Vector3(0.2f, 0.1f, 0), Quaternion.Euler(0, 0, shotAngle * Mathf.Rad2Deg));
                cloned.GetComponent<QuickCircleCollider>().Layer = QuickColliderLayer.EnemyProjectile;
                cloned.GetComponent<QuickCircleCollider>().SkipList.Add(this.GetComponent<QuickCircleCollider>());
                //cloned.GetComponent<LinearMove>().Velocity = dir.normalized * (5f - Math.Abs(3.5f - idx) * 0.75f);
                cloned.GetComponent<LinearMove>().Velocity = _align.normalized * ShotSpeed;
                cloned.GetComponent<Lifetime>().SetLifetime(5f);

                FireVibration fv = this.GetComponent<FireVibration>();
                if (fv != null)
                    fv.Fire();

                shotCount++;
            }

            if (shotCount == Magazine)
            {
                shotCount = 0;
                return true;
            }
            return false;
        }

        Vector2 RotateVector(Vector2 vec, float _angle)
        {
            var angle = Mathf.Deg2Rad * _angle;
            var result = new Vector2(vec.x * Mathf.Cos(angle) - vec.y * Mathf.Sin(angle),
                vec.y * Mathf.Cos(angle) + vec.x * Mathf.Sin(angle));
            return result;
        }
    }
}
