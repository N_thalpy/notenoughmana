﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;

using UnityEngine;
using System;
using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class SimpleAI : MonsterScriptBase
    {
        private bool isFlyingFinished;

        private Vector3 targetPos;
        private float timer;

        private float cooldownTimer;
        protected override void Start()
        {
            base.Start();
            timer = 0;
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());

            isFlyingFinished = false;
            transform.position = new Vector3(12 * (rd.NextFloat() - 0.5f), 4 + 2 * rd.NextFloat(), 0);
            targetPos = new Vector3(4 * (rd.NextFloat() - 0.5f), 2 * rd.NextFloat() + 1, 0);
        }

        protected override void Update()
        {
            base.Update();

            transform.position += 0.015f * Mathf.Cos(timer) * Vector3.right;
            timer += Time.deltaTime;

            if (isFlyingFinished == false)
            {
                if ((transform.position - targetPos).magnitude < 0.1)
                {
                    transform.position = targetPos;
                    isFlyingFinished = true;
                }

                transform.position = Vector3.Lerp(transform.position, targetPos, 0.03f);
            }

            cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);

            if (InitialCooldown > 0)
                return;


            GameObject prefab = Resources.Load<GameObject>("withtrail");
            Debug.Assert(prefab != null, "Prefab 'withtrail', for SimpleAI, has not found");
            if (cooldownTimer == 0)
            {
                cooldownTimer += 1.2f;

                Vector2 align = Camera.main.ScreenToWorldPoint(Input.mousePosition).XY() - transform.position.XY();
                Vector2 dir = PlayerScript.GetPlayerPosition().XY() - transform.position.XY();
                //Vector2 dir = (Quaternion.Euler(0, 0, deltaAngle) * align.XY0()).XY();

                float angle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                GameObject cloned = GameObject.Instantiate(prefab, transform.position, Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg));
                cloned.GetComponent<QuickCircleCollider>().Layer = QuickColliderLayer.EnemyProjectile;
                cloned.GetComponent<LinearMove>().Velocity = dir.normalized * 2f;
                cloned.GetComponent<Lifetime>().SetLifetime(5f);
            }

        }
    }
}
