﻿using NotEnoughMana.Mob;
using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.Control;

using UnityEngine;
using System;
using Random = System.Random;

namespace NotEnoughMana.Assets.Scripts.Mob.Impl
{
    public sealed class PopupAI : MonsterScriptBase
    {
        public GameObject PopupEffect;

        private float timer;

        private float cooldownTimer;
        protected override void Start()
        {
            base.Start();
            timer = 0;
        }
        public override void Knockback(float dist)
        {
            //Needs fix, Smooth movement required
            Vector2 dir = PlayerScript.GetPlayerPosition().XY() - transform.position.XY();

            transform.position -= (Vector3)dir.normalized * dist;
        }

        public override void Initialize()
        {
            Random rd = new Random(this.GetHashCode());
            transform.position = new Vector3(4 * (rd.NextFloat() - 0.5f), 2 * rd.NextFloat() - 1, 0);

            ManaOrbCount = 4;

            if (PopupEffect != null)
                GameObject.Instantiate(PopupEffect, transform.position, Quaternion.identity);
        }

        protected override void Update()
        {
            base.Update();

            transform.position += 0.015f * (Mathf.Cos(timer * 3) + Mathf.Sin(timer * 7)) * Vector3.right;


            if (InitialCooldown > 0)
                return;

            timer += Time.deltaTime;

            cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);

            GameObject prefab = Resources.Load<GameObject>("basic_f");
            Debug.Assert(prefab != null, "Prefab 'withtrail', for SimpleAI, has not found");
            if (cooldownTimer == 0)
            {
                cooldownTimer += 1f;

                Vector2 align = Camera.main.ScreenToWorldPoint(Input.mousePosition).XY() - transform.position.XY();
                Vector2 dir = PlayerScript.GetPlayerPosition().XY() - transform.position.XY();
                //Vector2 dir = (Quaternion.Euler(0, 0, deltaAngle) * align.XY0()).XY();

                float angle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                GameObject cloned = GameObject.Instantiate(prefab, transform.position, Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg));
                cloned.GetComponent<QuickCircleCollider>().Layer = QuickColliderLayer.EnemyProjectile;
                cloned.GetComponent<LinearMove>().Velocity = dir.normalized * 2f;
                cloned.GetComponent<Lifetime>().SetLifetime(5f);
            }

        }
    }
}
