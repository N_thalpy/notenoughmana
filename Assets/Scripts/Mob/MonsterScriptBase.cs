﻿using NotEnoughMana.QuickCollision;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

namespace NotEnoughMana.Mob
{
    public abstract class MonsterScriptBase : QuickCircleCollider
    {
        [Serializable]
        public struct ItemInfo
        {
            [SerializeField]
            public GameObject item;
            [SerializeField]
            public float rate;
        }

        public int MaxHP;
        public int CurrentHP;

        public int Defense;

        public int ManaOrbCount;

        public float InitialCooldown;

        public AudioClip DeathSound;

        public List<ItemInfo> DropTable;

        protected override void Start()
        {
            base.Start();
            CurrentHP = MaxHP;

            Layer = QuickColliderLayer.DoodadOrEnemy;
        }
        protected virtual void Update()
        {
            if (InitialCooldown > 0)
                InitialCooldown -= Time.deltaTime;

            if (CurrentHP <= 0)
            {
                ProcessDestroy();
                GameObject.Destroy(this.gameObject);
            }
        }
        public virtual void Knockback(float dist)
        {

        }
        public abstract void Initialize();

        public override void OnCollision(QuickColliderBase opp)
        {
            switch (opp.Layer)
            {
                case QuickColliderLayer.PlayerProjectile:
                    break;

                default:
                    break;
            }
        }

        protected virtual bool CanShot { get { return transform.position.y <= 4.5; } }

        void DropItem()
        {
            Random rd = new Random();
            if (DropTable.Count != 0) {
                var obj = DropTable.OrderBy(_ => rd.NextDouble()).First();
                if (rd.NextFloat() < obj.rate) {
                    GameObject.Instantiate(obj.item, transform.position, Quaternion.identity);
                }
            }

            GameObject manaOrb = Resources.Load<GameObject>("manaorb");
            for (int idx = 0; idx < ManaOrbCount; idx++)
                GameObject.Instantiate(manaOrb, transform.position + new Vector3(rd.NextFloat() - 0.5f, rd.NextFloat() - 0.5f, 0), Quaternion.identity);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        protected virtual void ProcessDestroy()
        {
            if (DeathSound != null) {
                var ad = GameObject.Find("BackGroundMusic").GetComponent<AudioSource>();
                if (ad != null) {
                    ad.PlayOneShot(DeathSound);
                }
            }
            
            DropItem();
        }
    }
}
