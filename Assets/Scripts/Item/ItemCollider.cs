﻿using NotEnoughMana.Control;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.QuickCollision.Impl;
using UnityEngine;

namespace NotEnoughMana.Item
{
    public sealed class ItemCollider : QuickCircleCollider
    {
        public float MaxSpeed;
        public float Accelation;

        public RingEnum RingEnum;
        public WeaponEnum WeaponEnum;

        private float speed;

        protected override void Awake()
        {
            Debug.Assert(MaxSpeed > 0);
            Debug.Assert(Accelation > 0);

            Debug.Assert(RingEnum != RingEnum.None || WeaponEnum != WeaponEnum.None);

            base.Awake();
            if (transform.position.y < 0)
                speed = -Mathf.Sqrt(-2 * transform.position.y * Accelation);
        }

        private void Update()
        {
            speed += Accelation * Time.deltaTime;
            if (MaxSpeed < speed)
                speed = MaxSpeed;

            transform.position += speed * Vector3.down * Time.deltaTime;
        }

        public override void OnCollision(QuickColliderBase opp)
        {
            if (opp is PlayerManaCollider && Input.GetKeyDown(KeyCode.E) == true)
                GameObject.Destroy(this.gameObject);
        }
    }
}
