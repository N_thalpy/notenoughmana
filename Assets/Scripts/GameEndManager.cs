﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEndManager : MonoBehaviour
{
    private static GameEndManager instance;
    public static GameEndManager Instance
    {
        get
        {
            if (!instance)
            {
                GameObject container = new GameObject();
                container.name = "GameEndManager";
                instance = container.AddComponent(typeof(GameEndManager)) as GameEndManager;
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }

    // Use this for initialization
    void Start () {
        GameEndManager.instance = this;
        isGameEnd = false;
        timer = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {

        if (isGameEnd)
        {
            if(timer > 0.0f)
            {
                timer -= Time.deltaTime;
            }

            if(timer <= 0.0f && Input.anyKey)
            {
                SceneManager.LoadScene("CollisionTest");
            }
        }
    }

    public void ShowRetryMessage()
    {
        isGameEnd = true;
        RetryMessage.SetActive(true);
    }

    public void ShowWinMessage()
    {
        isGameEnd = true;
        WinMessage.SetActive(true);
    }

    public GameObject RetryMessage;
    public GameObject WinMessage;

    private bool isGameEnd;
    private float timer;
}
