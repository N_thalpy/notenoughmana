﻿using NotEnoughMana.ExtensionMethod;
using NotEnoughMana.Projectile;
using NotEnoughMana.QuickCollision;
using NotEnoughMana.QuickCollision.Impl;
using NotEnoughMana.UI;
using System;
using UnityEngine;

namespace NotEnoughMana.Control
{
    public enum WeaponEnum
    {
        None,
        Basic,
        ShadowNova,
        FrozenOculus,
        InfernoStaff,
        VividBead,
    }
    public enum RingEnum
    {
        None,

        BlackholeRing,
        LocusOfPower,
        RefresherAmulet,
        TurningTide,
        LivingShadow,
    }
    public class PlayerScript : MonoBehaviour
    {
        public static GameObject Instance;

        public WeaponEnum SpareWeapon;
        public WeaponEnum CurrentWeapon;
        public RingEnum CurrentRing;

        private float absorbRange = 300f;
        private float absorbMultiplier = 2.0f;
        private float _chainCastLimit = 2f;
        private float _absorbTimer;
        private float _absorbCooldown = 2.3f;
        private float _manaRegen = 0f;

        public float Mana;
        private float health;
        public float Health
        {
            get
            {
                return health;
            }
            set
            {
                health = value;
                if(health <= 0)
                {
                    GameEndManager.Instance.ShowRetryMessage();
                    //Destroy(this.gameObject);
                }
            }
        }
        private Vector2 moveVector;
        private SpriteRenderer image;

        private PlayerAbsorbCollider _absCol;
        private PlayerInterceptCollider _itcCol;

        private float cooldownTimer;
        private float _bombTimer;

        public GameObject HPBar;
        public GameObject MPBar;
        public GameObject ChainCastBar;
        public GameObject AbsorbTimerUI;
        public GameObject BombTimerUI;
        private float _chainCastTimer;        
        private int _chainCast;

        public AudioClip ShootSound;
        public AudioClip VividSound;
        public AudioClip BombSound;
        public AudioClip CrashSound;

        private AudioSource audioSource;

        public void AbsorbProjectile()
        {
            Mana += absorbMultiplier;
        }

        public static Vector3 GetPlayerPosition()
        {
            return Instance.transform.position;
        }

        private void Start()
        {
            UIManager.Instance.Init();
            Health = 60;
            Mana = 80;
            _absorbTimer = 0f;
            _bombTimer = 0f;
            _chainCast = 0;
            Instance = gameObject;
            image = gameObject.GetComponent<SpriteRenderer>();
            _absCol = gameObject.GetComponentInChildren<PlayerAbsorbCollider>();
            _itcCol = gameObject.GetComponentInChildren<PlayerInterceptCollider>();

            //UIManager.Instance.SetMainWeapon(CurrentWeapon);
            //UIManager.Instance.SetSubWeapon(SpareWeapon);

            GetComponent<QuickCircleCollider>().Layer = QuickColliderLayer.PlayerUnit;

            //temp
            //SpareWeapon = WeaponEnum.FrozenOculus;
            //CurrentRing = RingEnum.RefresherAmulet;
            //CurrentRing = RingEnum.BlackholeRing;

            audioSource = gameObject.AddComponent<AudioSource>();
        }
        private void Update()
        {
            _chainCastTimer -= Time.deltaTime;
            _absorbTimer -= Time.deltaTime;

            if(_chainCastTimer<0f)
            {
                _chainCastTimer = 0f;
                _chainCast = 0;
                UIManager.Instance.ResetChain();
            }

            Move();
            UpdateWeapon();
            UpdateRing();
            UpdateUI();
            // Temporary mana gen
            if (Mana < 120)
                Mana += _manaRegen * Time.deltaTime;
            if (Mana > 120)
                Mana = 120;
        }
        private void UpdateUI()
        {
            MPBar.GetComponent<EnergyBar>().valueCurrent = (int)Mana;
            HPBar.GetComponent<EnergyBar>().valueCurrent = (int)Health;
            ChainCastBar.GetComponent<EnergyBar>().valueCurrent = (int)((_chainCastTimer / _chainCastLimit) * 100);
            AbsorbTimerUI.GetComponent<EnergyBar>().valueCurrent = (int)(_absorbTimer*10f);
            BombTimerUI.GetComponent<EnergyBar>().valueCurrent = (int)(_bombTimer * 10f);
        }
        private void Move()
        {
            moveVector = Vector2.zero;

            if (Input.GetKey(ControlConstant.MoveUp))
                moveVector.y += 1.5f;
            if (Input.GetKey(ControlConstant.MoveLeft))
                moveVector.x -= 1.5f;
            if (Input.GetKey(ControlConstant.MoveDown))
                moveVector.y -= 1.5f;
            if (Input.GetKey(ControlConstant.MoveRight))
                moveVector.x += 1.5f;

            if (moveVector.x < 0)
                image.flipX = true;
            else if (moveVector.x > 0)
                image.flipX = false;

            if (moveVector != Vector2.zero)
            {
                float currentSpeed = 4.0f;
                Vector3 dp = moveVector.normalized * currentSpeed * Time.deltaTime;
                if (gameObject.transform.position.x > 3) dp.x = Math.Min(dp.x, 0);
                if (gameObject.transform.position.y > 3.5) dp.y= Math.Min(dp.y, 0);
                if (gameObject.transform.position.x < -3) dp.x = Math.Max(dp.x, 0);
                if (gameObject.transform.position.y < -3.5) dp.y = Math.Max(dp.y, 0);
                gameObject.transform.position += dp;
            }
            
            if(Input.GetKeyUp(KeyCode.Q))
            {
                WeaponEnum t = CurrentWeapon;
                CurrentWeapon = SpareWeapon;
                SpareWeapon = t;

                _chainCast = 0;
                UIManager.Instance.UpdateChain(_chainCast);

                UIManager.Instance.SetMainWeapon(CurrentWeapon);
                UIManager.Instance.SetSubWeapon(SpareWeapon);
            }
            /*
            if (Input.GetMouseButton(1) == true && _absorbTimer < 0 )
            {
                _absCol.Activate();
                _absorbTimer = _absorbCooldown;
            }
            */
        }
        public void ResetChain()
        {
            _chainCast = 0;
            UIManager.Instance.UpdateChain(_chainCast);
        }

        private void Chain()
        {
            _chainCastTimer = _chainCastLimit;
            _chainCast++;
            UIManager.Instance.UpdateChain(_chainCast);
        }

        float vividTimer = 0f;
        int vividCount = 0;

        private void UpdateWeapon()
        {
            GetComponent<Animator>().SetBool("Attacking", Input.GetMouseButton(0));

            switch (CurrentWeapon)
            {
                case WeaponEnum.None:
                    break;

                case WeaponEnum.Basic: {
                        const float vbCooltime = 0.5f;
                        const int vbManaCost = 4;
                        const float vbVelocity = 7f;
                        const float vbLifetime = 5f;
                        cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);
                        _bombTimer = Math.Max(_bombTimer - Time.deltaTime, 0);

                        if (Input.GetMouseButton(0) == true && cooldownTimer == 0 && Mana > vbManaCost) {
                            cooldownTimer += Mathf.Max(0.35f, vbCooltime - (_chainCast*0.01f));
                            Mana -= vbManaCost;

                            audioSource.PlayOneShot(ShootSound);
                            
                            var rd = new System.Random();
                            GameObject prefab = Resources.Load<GameObject>("basic_long_f1");
                            Debug.Assert(prefab != null, "Prefab 'basic_long_f1', for WeaponEnum.Basic, has not found");

                            Chain();
                            Vector2 align = Camera.main.ScreenToWorldPoint(Input.mousePosition).XY() - transform.position.XY();
                            
                            Vector2 dir = align;
                            
                            float shotAngle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                            GameObject cloned = GameObject.Instantiate(prefab, transform.position, Quaternion.Euler(0, 0, shotAngle * Mathf.Rad2Deg));
                            QuickCircleCollider cc = cloned.GetComponent<QuickCircleCollider>();
                            cc.Layer = QuickColliderLayer.PlayerProjectile;
                            cc.SkipList.Add(this.GetComponent<QuickCircleCollider>());
                            if (CurrentRing == RingEnum.LocusOfPower) {
                                cc.Offset *= 2;
                                cc.Radius *= 2;
                                cloned.transform.localScale *= 2;
                            }

                            cloned.GetComponent<DamageAndDestroyOnCollision>().Damage = 3 + Math.Min(1, _chainCast / 15);
                            cloned.GetComponent<LinearMove>().Velocity = dir.normalized * vbVelocity;
                            cloned.GetComponent<Lifetime>().SetLifetime(vbLifetime);
                        }

                        if (Input.GetKeyDown(KeyCode.Space) == true && _bombTimer == 0) {
                            _bombTimer = 3f;

                            audioSource.PlayOneShot(VividSound);
                            GameObject prefab = Resources.Load<GameObject>("InfernoStaffProj");
                            Debug.Assert(prefab != null, "Prefab 'InfernoStaffProj', for WeaponEnum.Basic, has not found");

                            Chain();
                            Vector2 align = Camera.main.ScreenToWorldPoint(Input.mousePosition).XY() - transform.position.XY();
                            Vector2 dir = align;

                            float shotAngle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                            GameObject cloned = GameObject.Instantiate(prefab, transform.position, Quaternion.Euler(0, 0, shotAngle * Mathf.Rad2Deg));
                            QuickCircleCollider cc = cloned.GetComponent<QuickCircleCollider>();
                            cc.Layer = QuickColliderLayer.PlayerProjectile;
                            cc.SkipList.Add(this.GetComponent<QuickCircleCollider>());
                            if (CurrentRing == RingEnum.LocusOfPower) {
                                cc.Offset *= 2;
                                cc.Radius *= 2;
                                cloned.transform.localScale *= 2;
                            }
                            
                            cloned.GetComponent<LinearMove>().Velocity = dir.normalized * vbVelocity;
                            cloned.GetComponent<Lifetime>().SetLifetime(vbLifetime);
                        }
                    }
                    break;

                case WeaponEnum.ShadowNova:
                    {
                        // Constants for modify shadow nova
                        const float shadowNovaCooltime = 0.2f;
                        const int shadowNovaSpreadAngle = 15;
                        const int shadowNovaMana = 3;
                        const float shadowNovaVelocity = 5f;
                        const float shadowNovaLifetime = 0.75f;

                        cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);
                        _bombTimer = Math.Max(_bombTimer - Time.deltaTime, 0);

                        if(_bombTimer <=7f)
                            GetComponent<PlayerHitCollider>().Active = true;

                        GameObject prefab = Resources.Load<GameObject>("withtrail");
                        Debug.Assert(prefab != null, "Prefab 'withtrail', for WeaponEnum.ShadowNova, has not found");

                        if (Input.GetMouseButton(0) == true && cooldownTimer == 0 && Mana > shadowNovaMana)
                        {
                            cooldownTimer += shadowNovaCooltime - (0.01f * Math.Min(15,_chainCast));
                            Mana -= shadowNovaMana;
                            Chain();

                            audioSource.PlayOneShot(ShootSound);

                            Vector2 align = Camera.main.ScreenToWorldPoint(Input.mousePosition).XY() - transform.position.XY();

                            for (int idx = 0; idx < 8; idx++)
                            {
                                float deltaAngle = (idx - 7f / 2) * shadowNovaSpreadAngle / 4;
                                Vector2 dir = (Quaternion.Euler(0, 0, deltaAngle) * align.XY0()).XY();

                                float angle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                                GameObject cloned = GameObject.Instantiate(prefab, transform.position + new Vector3(0.4375f, 0f, 0f) - new Vector3(0.125f * idx + 0f, 0f), Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg));
                                QuickCircleCollider cc = cloned.GetComponent<QuickCircleCollider>();
                                cc.Layer = QuickColliderLayer.PlayerProjectile;
                                cc.SkipList.Add(this.GetComponent<QuickCircleCollider>());
                                if (CurrentRing == RingEnum.LocusOfPower)
                                {
                                    cc.Offset *= 2;
                                    cc.Radius *= 2;
                                    cloned.transform.localScale *= 2;
                                }                                

                                cloned.GetComponent<LinearMove>().Velocity = dir.normalized * (shadowNovaVelocity - Math.Abs(3.5f - idx) * 0.75f);
                                cloned.GetComponent<Lifetime>().SetLifetime(shadowNovaLifetime);
                                
                            }
                        }
                        if (Input.GetKeyDown(KeyCode.Space) == true && _bombTimer == 0)
                        {
                            _bombTimer = 10f;
                            GameObject shield = Resources.Load<GameObject>("ice_wall");                            
                            GameObject.Instantiate(shield, transform.position + new Vector3(0f, -0.35f, -0.5f),new Quaternion()).transform.parent = gameObject.transform;
                            GetComponent<PlayerHitCollider>().Active = false;


                        }

                    }
                    break;

                case WeaponEnum.FrozenOculus:
                    {
                        const float foCooltime = 0.5f;
                        const int foManaCost = 12;
                        const float foVelocity = 8f;
                        const float foLifetime = 20f;

                        const float foBombCool = 5f;

                        cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);
                        _bombTimer = Math.Max(_bombTimer - Time.deltaTime, 0);

                        GameObject prefab = Resources.Load<GameObject>("frozenOculusProj");
                        Debug.Assert(prefab != null, "Prefab 'withtrail', for WeaponEnum.FrozenOculus, has not found");
                        if (Input.GetMouseButton(0) == true && cooldownTimer == 0 && Mana > foManaCost)
                        {
                            cooldownTimer += foCooltime;
                            Mana -= foManaCost;                            
                            Chain();

                            audioSource.PlayOneShot(ShootSound);

                            Vector2 align = Camera.main.ScreenToWorldPoint(Input.mousePosition).XY() - transform.position.XY();
                            Vector2 dir = align;
                            //Vector2 dir = (Quaternion.Euler(0, 0, deltaAngle) * align.XY0()).XY();

                            float angle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                            GameObject cloned = GameObject.Instantiate(prefab, transform.position, Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg));
                            QuickCircleCollider cc = cloned.GetComponent<QuickCircleCollider>();
                            cc.SkipList.Add(this.GetComponent<QuickCircleCollider>());
                            if (CurrentRing == RingEnum.LocusOfPower)
                            {
                                cc.Offset *= 2;
                                cc.Radius *= 2;
                                cloned.transform.localScale *= 2;
                            }

                            cloned.GetComponent<DamageAndDestroyOnCollision>().Damage = 2 + Math.Min(5, _chainCast / 5);
                            cloned.GetComponent<LinearMove>().Velocity = dir.normalized * foVelocity;
                            cloned.GetComponent<Lifetime>().SetLifetime(foLifetime);
                        }

                        if (Input.GetKeyDown(KeyCode.Space) == true && _bombTimer == 0) {
                            audioSource.PlayOneShot(BombSound);
                            _bombTimer = 10f;

                            Chain(); Chain(); Chain(); Chain(); Chain();

                            _itcCol.Activate();
                        }
                    }
                    break;

                case WeaponEnum.InfernoStaff:
                    {
                        const float isCooltime = 1.25f;
                        const int isManaCost = 41;
                        const float isVelocity = 8f;
                        const float isLifetime = 20f;
                        const float infernoAngle = 90f;
                        const int bullets = 5;

                        cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);
                        _bombTimer = Math.Max(_bombTimer - Time.deltaTime, 0);

                        GameObject prefab = Resources.Load<GameObject>("InfernoStaffProj");
                        Debug.Assert(prefab != null, "Prefab 'InfernoStaffProj', for WeaponEnum.InfernoStaff, has not found");
                        if (Input.GetMouseButton(0) == true && cooldownTimer == 0 && Mana > isManaCost) {
                            cooldownTimer += isCooltime;
                            Mana -= isManaCost;
                            Chain();

                            audioSource.PlayOneShot(ShootSound);

                            Vector2 align = Camera.main.ScreenToWorldPoint(Input.mousePosition).XY() - transform.position.XY();

                            for (int idx = 0; idx < bullets; idx++) {
                                float deltaAngle = -45 + (infernoAngle / (bullets-1)) * idx;
                                Vector2 infdir = (Quaternion.Euler(0, 0, deltaAngle) * align.XY0()).XY();

                                float infang = Mathf.Atan2(infdir.y, infdir.x) - Mathf.PI / 2;

                                if(_chainCast > 2) {
                                    if(idx != 2) {
                                        continue;
                                    }
                                }

                                GameObject infclone = GameObject.Instantiate(prefab, transform.position + new Vector3(0.4375f, 0f, 0f) - new Vector3(0.125f * idx + 0f, 0f), Quaternion.Euler(0, 0, infang * Mathf.Rad2Deg));
                                QuickCircleCollider infcc = infclone.GetComponent<QuickCircleCollider>();
                                infcc.Layer = QuickColliderLayer.PlayerProjectile;
                                infcc.SkipList.Add(this.GetComponent<QuickCircleCollider>());
                                if (CurrentRing == RingEnum.LocusOfPower) {
                                    infcc.Offset *= 2;
                                    infcc.Radius *= 2;
                                    infclone.transform.localScale *= 2;
                                }

                                if(_chainCast >= 2) {
                                    infcc.Offset *= (_chainCast + 1) * 0.5f;
                                    infcc.Radius *= (_chainCast + 1) * 0.5f;
                                    infclone.transform.localScale *= (_chainCast + 1) * 0.5f;
                                    infclone.GetComponent<DamageAndDestroyOnCollision>().Damage += 7 * (_chainCast - 1);
                                }

                                infclone.GetComponent<LinearMove>().Velocity = infdir.normalized * isVelocity;
                                infclone.GetComponent<Lifetime>().SetLifetime(isLifetime);

                            }
                        }

                        if (Input.GetKeyDown(KeyCode.Space) == true && _bombTimer == 0) {
                            audioSource.PlayOneShot(BombSound);
                            _bombTimer = 10f;

                            _chainCast = 0;
                            UIManager.Instance.UpdateChain(_chainCast);

                            Mana = 120;
                        }
                    }
                    break;

                case WeaponEnum.VividBead:
                    {
                        const float vbCooltime = 0.1f;
                        const int vbManaCost = 2;
                        const float vbVelocity = 13f;
                        const float vbLifetime = 5f;
                        cooldownTimer = Math.Max(cooldownTimer - Time.deltaTime, 0);
                        _bombTimer = Math.Max(_bombTimer - Time.deltaTime, 0);
                        vividTimer = Math.Max(vividTimer - Time.deltaTime, 0);
                        
                        if (Input.GetMouseButton(0) == true && cooldownTimer == 0 && Mana > vbManaCost) {
                            cooldownTimer += vbCooltime;
                            Mana -= vbManaCost;

                            audioSource.PlayOneShot(VividSound);

                            int shots = 1;
                            if(vividTimer > 0) {
                                shots++;
                            }
                            int prev = -1;
                            float prevF = 80f;

                            for (int i = 0; i < shots; i++) {
                                var rd = new System.Random();
                                var rNum = vividCount++;
                                if(vividCount >= 4) {
                                    vividCount = 0;
                                }
                                GameObject prefab;
                                switch (rNum) {
                                    case 0:
                                        prefab = Resources.Load<GameObject>("PlayerBigIce");
                                        Debug.Assert(prefab != null, "Prefab 'PlayerBigIce', for WeaponEnum.VividBead, has not found");
                                        break;
                                    case 1:
                                        prefab = Resources.Load<GameObject>("PlayerBasicFire");
                                        Debug.Assert(prefab != null, "Prefab 'PlayerBasicFire', for WeaponEnum.VividBead, has not found");
                                        break;
                                    case 2:
                                        prefab = Resources.Load<GameObject>("PlayerBigWind");
                                        Debug.Assert(prefab != null, "Prefab 'PlayerBigWind', for WeaponEnum.VividBead, has not found");
                                        break;
                                    case 3:
                                    default:
                                        prefab = Resources.Load<GameObject>("PlayerBigOrange");
                                        Debug.Assert(prefab != null, "Prefab 'PlayerBigOrange', for WeaponEnum.VividBead, has not found");
                                        break;
                                }
                                
                                Chain();
                                Vector2 align = Camera.main.ScreenToWorldPoint(Input.mousePosition).XY() - transform.position.XY();
                                float angle = (rd.NextFloat() * 20f) - 10f;
                                if (i == 1) {
                                    angle = (rd.NextFloat() * 20f) - 10f;
                                }
                                Vector2 dir = RotateVector(align, angle);                                

                                Debug.Log(rNum + " : " + angle + " " + dir);

                                float shotAngle = Mathf.Atan2(dir.y, dir.x) - Mathf.PI / 2;

                                GameObject cloned = GameObject.Instantiate(prefab, transform.position, Quaternion.Euler(0, 0, shotAngle * Mathf.Rad2Deg));
                                QuickCircleCollider cc = cloned.GetComponent<QuickCircleCollider>();
                                cc.Layer = QuickColliderLayer.PlayerProjectile;
                                cc.SkipList.Add(this.GetComponent<QuickCircleCollider>());
                                if (CurrentRing == RingEnum.LocusOfPower) {
                                    cc.Offset *= 2;
                                    cc.Radius *= 2;
                                    cloned.transform.localScale *= 2;
                                }

                                cloned.GetComponent<DamageAndDestroyOnCollision>().Damage = 1 + Math.Min(2, _chainCast/30);
                                cloned.GetComponent<LinearMove>().Velocity = dir.normalized * vbVelocity;
                                cloned.GetComponent<Lifetime>().SetLifetime(vbLifetime);

                                prev = rNum;
                                prevF = angle;
                            }
                        }

                        if (Input.GetKeyDown(KeyCode.Space) == true && _bombTimer == 0) {
                            audioSource.PlayOneShot(BombSound);

                            _bombTimer = 10f;
                            vividTimer = 3.0f;
                        }
                    }
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        Vector2 RotateVector(Vector2 vec, float _angle)
        {
            var angle = Mathf.Deg2Rad * _angle;
            var result = new Vector2(vec.x * Mathf.Cos(angle) - vec.y * Mathf.Sin(angle),
                vec.y * Mathf.Cos(angle) + vec.x * Mathf.Sin(angle));
            return result;
        }

        public static void PlayCrashSound(float volumeScale = 1.0f)
        {
            Instance.GetComponent<PlayerScript>().playCrashSound(volumeScale);
        }

        private void playCrashSound(float volumeScale = 1.0f)
        {
            audioSource.PlayOneShot(CrashSound, volumeScale);
        }

        private void UpdateRing()
        {
            switch (CurrentRing)
            {
                case RingEnum.None:
                    if (Input.GetMouseButton(1) == true && _absorbTimer < 0) {
                        _absCol.Activate();
                        absorbMultiplier = 2;
                        _absorbTimer = _absorbCooldown;
                    }
                    break;

                case RingEnum.LocusOfPower:
                    if (Input.GetMouseButton(1) == true && _absorbTimer < 0)
                    {
                        _absCol.Activate(1f);
                        absorbMultiplier = 2;
                        _absorbTimer = _absorbCooldown;
                    }
                    break;

                case RingEnum.BlackholeRing:
                    {
                        if (Input.GetMouseButton(1) == true && _absorbTimer < 0)
                        {
                            _absCol.Activate(2f);
                            absorbMultiplier = 2;
                            _absorbTimer = _absorbCooldown;
                        }
                    }
                    break;

                case RingEnum.RefresherAmulet:
                    {
                        if (Mana < 100)
                            Mana += 30 * Time.deltaTime;
                    }
                    break;

                case RingEnum.TurningTide:
                    {
                        if (Mana <= 30)
                            absorbMultiplier = 6f;
                        else
                            absorbMultiplier = 2f;

                        if (Input.GetMouseButton(1) == true && _absorbTimer < 0)
                        {
                            _absCol.Activate(1f);
                            _absorbTimer = _absorbCooldown;
                        }
                    }
                    break;

                case RingEnum.LivingShadow:
                    {
                        if (Input.GetMouseButton(1) == true && _absorbTimer < 0 && Mana >= 20) {
                            _absorbTimer = _absorbCooldown;
                            
                            Mana = Math.Max(Mana - 30, 0);
                            Mana += 10;

                            absorbMultiplier = 1;
                            _absCol.Activate();

                            var prefab = Resources.Load<GameObject>("mob/friendTower");
                            Debug.Assert(prefab != null, "Prefab 'mob/friendTower' has not found");
                            var newFriend = GameObject.Instantiate(prefab);
                            var ai = newFriend.GetComponent<Assets.Scripts.Mob.Impl.FriendAI>();
                            ai.Initpos = transform.position;
                            ai.Initialize();
                            var col = newFriend.GetComponent<FriendManaCollider>();
                            col.PS = this;
                        }
                    }
                    break;

                default:
                    throw new NotImplementedException();
            }
        }
    }
}
