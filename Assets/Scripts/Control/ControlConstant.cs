﻿using UnityEngine;

namespace NotEnoughMana.Control
{
    public static class ControlConstant
    {
        public const KeyCode MoveLeft = KeyCode.A;
        public const KeyCode MoveRight = KeyCode.D;
        public const KeyCode MoveUp = KeyCode.W;
        public const KeyCode MoveDown = KeyCode.S;
    }
}
