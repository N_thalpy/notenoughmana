﻿using UnityEngine;

namespace NotEnoughMana.QuickCollision
{
    public abstract class QuickSquareCollider : QuickColliderBase
    {
        public Vector2 Offset;
        public Vector2 Length;
        public float Width;
        
        protected virtual void OnDrawGizmosSelected()
        {
            if (Active == true && Length.magnitude > 0 && Width > 0)
            {
                Gizmos.color = Color.yellow;

                Vector2 perp = Vector3.Cross(Vector3.forward, Length).normalized;
                // b -----> c
                // ^        |
                // |        |
                // |        |
                // |        v
                // a offset d
                //
                // Winding order: a -> b -> c -> d -> a

                Vector2 a = Offset + perp * Width;
                Vector2 b = a + Length;
                Vector2 c = b - 2 * perp * Width;
                Vector2 d = c - Length;
                
                Gizmos.DrawLine(transform.position + transform.rotation * a, transform.position + transform.rotation * b);
                Gizmos.DrawLine(transform.position + transform.rotation * b, transform.position + transform.rotation * c);
                Gizmos.DrawLine(transform.position + transform.rotation * c, transform.position + transform.rotation * d);
                Gizmos.DrawLine(transform.position + transform.rotation * d, transform.position + transform.rotation * a);
            }
        }
    }
}
