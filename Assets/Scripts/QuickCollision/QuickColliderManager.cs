﻿using NotEnoughMana.ExtensionMethod;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace NotEnoughMana.QuickCollision
{
    public class QuickColliderManager : MonoBehaviour
    {
        public static QuickColliderManager Instance;

        public List<QuickColliderBase> ColliderList;
        private List<QuickColliderBase> removeCandidate;

        private List<QuickColliderBase> colliderListA;
        private List<QuickColliderBase> colliderLayerB;

        private void Awake()
        {
            ColliderList = new List<QuickColliderBase>();
            removeCandidate = new List<QuickColliderBase>();

            colliderListA = new List<QuickColliderBase>();
            colliderLayerB = new List<QuickColliderBase>();

            Instance = this;
        }
        private void Update()
        {
            Debug.Assert(ColliderList.TrueForAll(_ => _.Layer != QuickColliderLayer.None));
            ColliderList.RemoveAll(_ => removeCandidate.Contains(_));
            removeCandidate.Clear();

            CheckCollision(QuickColliderLayer.PlayerUnit, QuickColliderLayer.EnemyProjectile);
            CheckCollision(QuickColliderLayer.PlayerUnit, QuickColliderLayer.DoodadOrEnemy);
            CheckCollision(QuickColliderLayer.PlayerUnit, QuickColliderLayer.ItemOrOrb);

            CheckCollision(QuickColliderLayer.DoodadOrEnemy, QuickColliderLayer.PlayerProjectile);
        }

        private void CheckCollision(QuickColliderLayer layerA, QuickColliderLayer layerB)
        {
            colliderListA.Clear();
            colliderLayerB.Clear();

            foreach (QuickColliderBase c in ColliderList)
            {
                if (c.Layer == layerA)
                    colliderListA.Add(c);
                if (c.Layer == layerB)
                    colliderLayerB.Add(c);
            }

            foreach (QuickColliderBase a in colliderListA)
                foreach (QuickColliderBase b in colliderLayerB)
                    if (a != b && a.Active == true && b.Active == true)
                    {
                        #region Circle-Circle collision check
                        if (a is QuickCircleCollider && b is QuickCircleCollider)
                            CheckCircleCircleCollision(a as QuickCircleCollider, b as QuickCircleCollider);
                        #endregion
                        #region Square-Square collision check
                        if (a is QuickSquareCollider && b is QuickSquareCollider)
                            CheckSquareSquareCollision(a as QuickSquareCollider, b as QuickSquareCollider);
                        #endregion
                        #region Circle-Square collision check
                        if (a is QuickCircleCollider && b is QuickSquareCollider)
                            CheckCircleSquareCollision(a as QuickCircleCollider, b as QuickSquareCollider);
                        if (a is QuickSquareCollider && b is QuickCircleCollider)
                            CheckCircleSquareCollision(b as QuickCircleCollider, a as QuickSquareCollider);
                        #endregion
                    }
        }

        private void CheckCircleCircleCollision(QuickCircleCollider a, QuickCircleCollider b)
        {
            float r2 = a.Radius + b.Radius;
            r2 = r2 * r2;

            Vector2 aV = a.transform.position.XY() + (a.transform.rotation * a.Offset).XY();
            Vector2 bV = b.transform.position.XY() + (b.transform.rotation * b.Offset).XY();

            if ((aV - bV).sqrMagnitude < r2)
            {
                if (a.SkipList.Contains(b) == false)
                    a.OnCollision(b);
                if (b.SkipList.Contains(a) == false)
                    b.OnCollision(a);
            }
        }
        private void CheckCircleSquareCollision(QuickCircleCollider a, QuickSquareCollider b)
        {
            bool collided = false;

            // CASE 1: center of circle is in square
            if (CheckPointInsideSquare(a.transform.position + a.transform.rotation * a.Offset, b) == true)
            {
                Debug.Log("case 1");
                collided = true;
            }

            // CASE 2: edge of square intersects w/ circle
            Vector2 perp = Vector3.Cross(Vector3.forward, b.Length).normalized;
            Vector2 pa = b.Offset + perp * b.Width;
            Vector2 pb = pa + b.Length;
            Vector2 pc = pb - 2 * perp * b.Width;
            Vector2 pd = pc - b.Length;

            if (CheckCircleLineIntersection(a, b.transform.position + b.transform.rotation * pa, b.transform.position + b.transform.rotation * pb) == true ||
                CheckCircleLineIntersection(a, b.transform.position + b.transform.rotation * pb, b.transform.position + b.transform.rotation * pc) == true ||
                CheckCircleLineIntersection(a, b.transform.position + b.transform.rotation * pc, b.transform.position + b.transform.rotation * pd) == true ||
                CheckCircleLineIntersection(a, b.transform.position + b.transform.rotation * pd, b.transform.position + b.transform.rotation * pa) == true)
            {
                Debug.Log("case 2");
                collided = true;
            }

            if (collided == true)
            {
                if (a.SkipList.Contains(b) == false)
                    a.OnCollision(b);
                if (b.SkipList.Contains(a) == false)
                    b.OnCollision(a);
            }
        }
        private void CheckSquareSquareCollision(QuickSquareCollider a, QuickSquareCollider b)
        {
            throw new NotImplementedException();
        }
        
        private bool CheckCircleLineIntersection(QuickCircleCollider circle, Vector2 from, Vector2 to)
        {
            Vector2 center = (circle.transform.rotation * circle.Offset).XY() + circle.transform.position.XY();

            Vector2 dir = (to - from).normalized;
            float maxdist = (to - from).magnitude;

            // minimize d = norm (from + t * dir - center)
            // del d/dt d implies t = [(center.x + center.y) - (from.x + from.y)] / (dir.x + dir.y) 

            float mint = ((center.x + center.y) - (from.x + from.y)) / (dir.x + dir.y);
            mint = Mathf.Clamp(mint, 0, maxdist);

            float mindist2 = (from - center + mint * dir).sqrMagnitude;
            return mindist2 < circle.Radius * circle.Radius;
        }
        private bool CheckPointInsideSquare(Vector2 point, QuickSquareCollider square)
        {
            Vector2 perp = Vector3.Cross(Vector3.forward, square.Length).normalized;
            Vector2 pa = square.Offset + perp * square.Width;
            Vector2 pb = pa + square.Length;
            Vector2 pc = pb - 2 * perp * square.Width;
            Vector2 pd = pc - square.Length;

            Vector2 vecAB = square.transform.rotation * (pb - pa);
            Vector2 vecAD = square.transform.rotation * (pd - pa);
            Vector2 vecAP = point - (square.transform.rotation * pa + square.transform.position).XY();

            float abap = Vector2.Dot(vecAB, vecAP);
            float adap = Vector2.Dot(vecAD, vecAP);

            if (0 < abap && abap < vecAB.sqrMagnitude && 0 < adap && adap < vecAD.sqrMagnitude)
                return true;
            return false;
        }

        public void Enqueue(QuickColliderBase coll)
        {
            ColliderList.Add(coll);
        }
        public void Dequeue(QuickColliderBase coll)
        {
            removeCandidate.Add(coll);
        }
    }
}
