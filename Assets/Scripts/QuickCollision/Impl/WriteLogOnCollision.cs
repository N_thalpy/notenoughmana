﻿using System;
using UnityEngine;

namespace NotEnoughMana.QuickCollision.Impl
{
    public class WriteLogOnCollision : QuickCircleCollider
    {
        public override void OnCollision(QuickColliderBase opp)
        {
            Debug.Log(String.Format("{0} has collided with {1}", this.name, opp.name));
        }
    }
}
