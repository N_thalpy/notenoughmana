﻿using NotEnoughMana.Control;
using NotEnoughMana.Item;
using NotEnoughMana.UI;
using UnityEngine;

namespace NotEnoughMana.QuickCollision.Impl
{
    public sealed class FriendManaCollider : QuickCircleCollider
    {
        public PlayerScript PS;

        public override void OnCollision(QuickColliderBase opp)
        {
            switch (opp.Layer)
            {
                case QuickColliderLayer.ItemOrOrb:
                    if (opp is ManaOrb)
                        // 10 is temporary value for test
                        Debug.Assert(PS != null, "No Player Script For Friend");
                        PS.Mana += 10;
                    break;

                default:
                    break;
            }
        }
    }
}
