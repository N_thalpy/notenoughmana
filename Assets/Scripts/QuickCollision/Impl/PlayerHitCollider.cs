﻿using NotEnoughMana.Control;
using NotEnoughMana.UI;
using UnityEngine;


namespace NotEnoughMana.QuickCollision.Impl
{
    public sealed class PlayerHitCollider : QuickCircleCollider
    {
        public override void OnCollision(QuickColliderBase opp)
        {
            switch (opp.Layer)
            {
                case QuickColliderLayer.EnemyProjectile:
                    UIManager.Instance.ShakeCamera(0.5f, 0.3f);
                    PlayerScript.PlayCrashSound();
                    break;
                case QuickColliderLayer.ItemOrOrb:
                    break;

                default:
                    Debug.Log("Collided w/ " + opp.name);
                    break;
            }
        }
    }
}
