﻿using NotEnoughMana.Mob;
using System;
using UnityEngine;

namespace NotEnoughMana.QuickCollision.Impl
{
    public sealed class Explosion : QuickCircleCollider
    {
        public int Damage;
        public GameObject HitEffect;


        protected override void Start()
        {
            base.Start();
            Debug.Assert(Damage > 0, "Damage should be gt. than zero");
        }
        public override void OnCollision(QuickColliderBase opp)
        {
            MonsterScriptBase mobScript = opp.transform.gameObject.GetComponent<MonsterScriptBase>();
            if (mobScript != null)
            {
                mobScript.CurrentHP -= Math.Max(1, Damage - mobScript.Defense);
                if (HitEffect != null)
                {
                    GameObject.Instantiate(HitEffect, gameObject.transform.position, new Quaternion());
                }

                SkipList.Add(opp);
                opp.SkipList.Add(this);
            }
        }
    }
}
