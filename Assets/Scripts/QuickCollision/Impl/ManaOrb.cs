﻿using UnityEngine;

namespace NotEnoughMana.QuickCollision.Impl
{
    public sealed class ManaOrb : QuickCircleCollider
    {
        private void Update()
        {
            transform.position -= new Vector3(0, 1f * Time.deltaTime, 0);

            if(transform.position.y < -10f) {
                Destroy(this.gameObject);
            }
        }

        public override void OnCollision(QuickColliderBase opp)
        {
            Debug.Assert(opp.Layer == QuickColliderLayer.PlayerUnit);

            if (opp is PlayerManaCollider)
                Destroy(this.gameObject);
            else if (opp is FriendManaCollider)
                Destroy(this.gameObject);
        }
    }
}
