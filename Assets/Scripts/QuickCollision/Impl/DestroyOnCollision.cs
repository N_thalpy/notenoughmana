﻿using UnityEngine;

namespace NotEnoughMana.QuickCollision.Impl
{
    public class DestroyOnCollision : QuickCircleCollider
    {
        public GameObject HitEffect;

        public override void OnCollision(QuickColliderBase opp)
        {
            if (opp is PlayerManaCollider)
                return;

            if (HitEffect != null)
                GameObject.Instantiate(HitEffect, gameObject.transform.position, new Quaternion());
            GameObject.Destroy(gameObject);            
        }
    }
}
