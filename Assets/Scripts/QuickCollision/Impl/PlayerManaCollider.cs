﻿using NotEnoughMana.Control;
using NotEnoughMana.Item;
using NotEnoughMana.UI;
using UnityEngine;

namespace NotEnoughMana.QuickCollision.Impl
{
    public sealed class PlayerManaCollider : QuickCircleCollider
    {
        public override void OnCollision(QuickColliderBase opp)
        {
            switch (opp.Layer)
            {
                case QuickColliderLayer.ItemOrOrb:
                    if (opp is ManaOrb)
                        // 10 is temporary value for test
                        GetComponent<PlayerScript>().Mana += 10;
                    if (opp is ItemCollider && Input.GetKey(KeyCode.E) == true)
                    {
                        ItemCollider item = opp as ItemCollider;
                        var ps = GetComponent<PlayerScript>();

                        if (item.WeaponEnum != WeaponEnum.None)
                        {
                            if (ps.CurrentWeapon != WeaponEnum.None && ps.SpareWeapon == WeaponEnum.None) {
                                ps.SpareWeapon = item.WeaponEnum;
                                UIManager.Instance.SetSubWeapon(item.WeaponEnum);
                            } else {
                                ps.ResetChain();
                                ps.CurrentWeapon = item.WeaponEnum;
                                UIManager.Instance.SetMainWeapon(item.WeaponEnum);
                            }
                        }
                        if (item.RingEnum != RingEnum.None)
                        {
                            ps.CurrentRing = item.RingEnum;
                            UIManager.Instance.SetAccessory(item.RingEnum);
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    }
}
