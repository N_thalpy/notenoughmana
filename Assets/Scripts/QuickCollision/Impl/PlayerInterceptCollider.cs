﻿using NotEnoughMana.Control;
using UnityEngine;

namespace NotEnoughMana.QuickCollision.Impl
{
    public sealed class PlayerInterceptCollider : QuickCircleCollider
    {
        public PlayerScript Player;
        private float _dur = 0.3f;
        private float _radiusBase = 100f;
        public void Activate(float rangeMult)
        {
            Debug.Log("ACTIVATED");
            gameObject.GetComponentInChildren<ParticleSystem>().gameObject.transform.localScale = new Vector3(rangeMult, rangeMult, rangeMult);
            gameObject.GetComponentInChildren<ParticleSystem>().Play();
            Radius = _radiusBase * rangeMult;
            Active = true;

        }
        public void Activate()
        {
            Debug.Log("ACTIVATED");
            gameObject.GetComponentInChildren<ParticleSystem>().Play();
            Active = true;
        }
        private void Update()
        {            
            if (Active == true)
            {
                _dur -= Time.deltaTime;
                if (_dur < 0)
                {
                    _dur = 0.3f;
                    Active = false;
                }
            }
        }
        public override void OnCollision(QuickColliderBase opp)
        {
            switch (opp.Layer)
            {
                case QuickColliderLayer.ItemOrOrb:
                    break;
                case QuickColliderLayer.EnemyProjectile:
                    break;
                default:
                    Debug.Log("Collided w/ " + opp.name);
                    break;
            }
        }
    }
}
