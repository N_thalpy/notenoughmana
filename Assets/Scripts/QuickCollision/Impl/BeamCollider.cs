﻿using NotEnoughMana.Control;
using NotEnoughMana.Mob;
using System;
using UnityEngine;

namespace NotEnoughMana.QuickCollision.Impl
{
    public sealed class BeamCollider : QuickSquareCollider
    {
        public int Damage;
        public GameObject HitEffect;

        protected override void Start()
        {
            base.Start();
            Debug.Assert(Damage > 0, "Damage should be gt. than zero");
        }
        public override void OnCollision(QuickColliderBase opp)
        {

            MonsterScriptBase mobScript = opp.transform.gameObject.GetComponent<MonsterScriptBase>();
            if (mobScript != null)
            {
                mobScript.CurrentHP -= Math.Max(1, Damage - mobScript.Defense);
                if (HitEffect != null)
                    GameObject.Instantiate(HitEffect, gameObject.transform.position, Quaternion.identity);
                
                return;
            }

            PlayerScript playerScript = opp.transform.gameObject.GetComponent<PlayerScript>();
            if (playerScript != null)
            {
                playerScript.Health -= 1;
                if (HitEffect != null)
                    GameObject.Instantiate(HitEffect, gameObject.transform.position, Quaternion.identity);
                
                return;
            }
        }
    }
}
