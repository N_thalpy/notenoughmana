﻿using System;
using UnityEngine;

namespace NotEnoughMana.QuickCollision.Impl
{
    public sealed class WriteLogOnCollisionSquare : QuickSquareCollider
    {
        public override void OnCollision(QuickColliderBase opp)
        {
            Debug.Log("Collided!!");
        }
    }
}
