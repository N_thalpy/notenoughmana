﻿using NotEnoughMana.ExtensionMethod;
using UnityEngine;

namespace NotEnoughMana.QuickCollision
{
    public abstract class QuickCircleCollider : QuickColliderBase
    {
        public float Radius;
        public Vector2 Offset;
                
        protected virtual void OnDrawGizmosSelected()
        { 
            if (Active == true && Radius > 0)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawWireSphere(transform.position.XY() + (gameObject.transform.rotation * Offset).XY(), Radius);
            }
        }
    }
}
