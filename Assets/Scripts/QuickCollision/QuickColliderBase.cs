﻿using System.Collections.Generic;
using UnityEngine;

namespace NotEnoughMana.QuickCollision
{
    public abstract class QuickColliderBase : MonoBehaviour
    {
        public bool Active;
        public QuickColliderLayer Layer;
        public List<QuickColliderBase> SkipList;

        protected virtual void Awake()
        {
            Active = true;
            SkipList = new List<QuickColliderBase>();
        }
        protected virtual void Start()
        {
            Debug.Assert(QuickColliderManager.Instance != null);
            QuickColliderManager.Instance.Enqueue(this);
        }
        protected virtual void OnDestroy()
        {
            QuickColliderManager.Instance.Dequeue(this);
        }

        public abstract void OnCollision(QuickColliderBase opp);
    }
}
