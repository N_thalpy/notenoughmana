﻿namespace NotEnoughMana.QuickCollision
{
    public enum QuickColliderLayer
    {
        None,

        ItemOrOrb,
        PlayerProjectile,
        EnemyProjectile,
        PlayerUnit,
        DoodadOrEnemy,
    }
}
