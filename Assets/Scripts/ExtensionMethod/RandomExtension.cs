﻿using System;

namespace NotEnoughMana
{
    public static class RandomExtension
    {
        public static float NextFloat(this Random rd)
        {
            return (float)rd.NextDouble();
        }
    }
}
