﻿using UnityEngine;

namespace NotEnoughMana.ExtensionMethod
{
    public static class VectorExtension
    {
        public static Vector2 XY(this Vector3 v)
        {
            return new Vector2(v.x, v.y);
        }
        public static Vector3 XY0(this Vector2 v)
        {
            return new Vector3(v.x, v.y, 0);
        }
    }
}
