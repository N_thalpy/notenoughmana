﻿using System;
using UnityEngine;

namespace NotEnoughMana.Assets.Scripts.Movement
{
    public sealed class BouncingMotion : MonoBehaviour
    {
        public float InitialSpeed;
        public float Gravity;
        public float Cooltime;

        private float speed;
        private float timer;
        private float dy;

        private void Awake()
        {
            speed = 0;
            timer = 0;
            dy = 0;
        }
        private void Update()
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                speed -= Gravity * Time.deltaTime;
                float newdy = dy + speed * Time.deltaTime;
                if (newdy < 0)
                {
                    newdy = 0;
                    timer = Cooltime;
                    speed = InitialSpeed;
                }

                transform.position += Vector3.up * (newdy - dy);
                dy = newdy;
            }
        }
    }
}
