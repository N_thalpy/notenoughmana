﻿using UnityEngine;

namespace NotEnoughMana.Assets.Scripts.Movement
{
    public sealed class FireVibration : MonoBehaviour
    {
        public float Amplication;
        public float MaxTime;

        public float[] XNoiseParam;
        public float[] YNoiseParam;

        private float timer;
        private float dx;
        private float dy;

        private void Update()
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
                float timeModifier = Mathf.PI * 2 / MaxTime;

                float newdx = 0;
                float newdy = 0;

                if (timer <= 0)
                {
                    newdx = 0;
                    newdy = 0;
                }
                else
                {
                    for (int idx = 0; idx < XNoiseParam.Length; idx++)
                        newdx += Amplication * XNoiseParam[idx] * Mathf.Sin((idx + 1) * timeModifier * timer);
                    for (int idx = 0; idx < YNoiseParam.Length; idx++)
                        newdy += Amplication * YNoiseParam[idx] * Mathf.Sin((idx + 1) * timeModifier * timer);
                }

                transform.position += new Vector3(newdx - dx, newdy - dy, 0);
                newdx = dx;
                newdy = dy;
            }
        }
        public void Fire()
        {
            if (timer <= 0)
                timer = MaxTime;
        }
    }
}
