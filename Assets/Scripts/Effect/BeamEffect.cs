﻿using UnityEngine;

namespace NotEnoughMana.Effect
{
    public class BeamEffect : MonoBehaviour
    {
        private GameObject beam;
        private LineRenderer line;

        public GameObject BeamPrefab;
        
        public float textureScrollSpeed = 8f; //How fast the texture scrolls along the beam
        public float textureLengthScale = 3; //Length of the beam texture

        private void Start()
        {
            beam = Instantiate<GameObject>(BeamPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            line = beam.GetComponent<LineRenderer>();
        }
        private void Update()
        {
            Vector3 start = transform.position;
            Vector3 end = transform.position + (transform.rotation * Vector2.up * 10);

            line.positionCount = 2;
            line.SetPosition(0, start);
            line.SetPosition(1, end);
            Debug.Log("updated");

            float distance = Vector3.Distance(start, end);
            line.sharedMaterial.mainTextureScale = new Vector2(distance / textureLengthScale, 1);
            line.sharedMaterial.mainTextureOffset -= new Vector2(Time.deltaTime * textureScrollSpeed, 0);
        }

        private void OnDestroy()
        {
            GameObject.Destroy(beam);
        }
    }
}
