﻿using NotEnoughMana.Mob;
using UnityEngine;

namespace NotEnoughMana.Map
{
    public sealed class MonsterTrigger : MonoBehaviour
    {
        private bool triggered;

        public MonsterCluster Cluster;
        public bool Triggered
        {
            get
            {
                return triggered;
            }
        }

        private void Awake()
        {
            triggered = false;
        }
        private void Update()
        {
            if (transform.position.y < 0 && triggered == false)
            {
                foreach (GameObject obj in Cluster.MonsterList)
                {
                    GameObject cloned = GameObject.Instantiate(obj);
                    cloned.GetComponent<MonsterScriptBase>().Initialize();
                }
                triggered = true;
            }
        }
    }
}
