﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;


using NotEnoughMana.Assets.Scripts.Mob.Impl;
namespace NotEnoughMana.Map
{
    public class MapGenerator : MonoBehaviour
    {
        [Serializable]
        public struct MapGeneratorData
        {
            [SerializeField]
            public SectorEnum SectorEnum;
            [SerializeField]
            public int Count;
        }

        public float FlowSpeed;

        public MapGeneratorData[] GeneratorData;

        /*
        public SectorEnum MidBossSectorEnum;
        public int MidBossIndex;
        */

        public SectorEnum LastBossSectorEnum;

        private List<GameObject> lastBossSectorTriggerList;


        static MapGenerator _instance;
        static public MapGenerator Instance
        {
            get
            {
                Debug.Assert(_instance != null);
                return _instance;
            }
        }

        private void Start()
        {
            Debug.Assert(_instance == null);
            _instance = this;

            Debug.Assert(FlowSpeed > 0, "Flow speed must be gt. than zero");

            Debug.Assert(GeneratorData.Length > 0, "Number of pool of sector must be gt. than zero");
            Debug.Assert(GeneratorData.All(_ => _.Count >= 0), "Number of sector must be g.eq. than zero");

            transform.position = Vector3.zero;

            List<SectorEnum> enumList = new List<SectorEnum>();
            foreach (MapGeneratorData dat in GeneratorData)
                for (int idx = 0; idx < dat.Count; idx++)
                    enumList.Add(dat.SectorEnum);

            Random rd = new Random();
            //enumList = enumList.OrderBy(_ => rd.NextDouble()).ToList();

            SectorData data;
            GameObject sectorResource;

            float heightAccum = 0;
            for (int idx = 0; idx < enumList.Count; idx++)
            {
                // TODO : mid-boss check;
                data = SectorDataFactory.GetRandomDataFromEnum(enumList[idx]);

                sectorResource = GameObject.Instantiate(data.ResourcePrefab, new Vector3(0, heightAccum-0.2f, 2), Quaternion.identity);
                sectorResource.transform.parent = this.transform;
                heightAccum += data.Height-0.2f;

                //for (int jdx = 0; jdx < data.MonsterCount; jdx++){
                //    GenerateMonsterTrigger(sectorResource, data.MonsterClusterList.OrderBy(_ => rd.NextDouble()).First());

                //시연용으로 랜덤 삭제.
                for (int jdx = 0; jdx < data.MonsterClusterList.Length; jdx++) {
                    GenerateMonsterTrigger(sectorResource, data.MonsterClusterList[jdx]);
                }

                if (data.SpeedChange != 0) {
                    GenerateSpeedTrigger(sectorResource, data.SpeedChange);
                }
            }

            // TODO : last-boss check
            data = SectorDataFactory.GetRandomDataFromEnum(LastBossSectorEnum);

            sectorResource = GameObject.Instantiate(data.ResourcePrefab, new Vector3(0, heightAccum-0.2f, 2), Quaternion.identity);
            sectorResource.transform.parent = this.transform;
            heightAccum += data.Height-0.2f;

            lastBossSectorTriggerList = new List<GameObject>();
            for (int jdx = 0; jdx < data.MonsterCount; jdx++)
                lastBossSectorTriggerList.Add(GenerateMonsterTrigger(sectorResource, data.MonsterClusterList.OrderBy(_ => rd.NextDouble()).First()));
        }

        private GameObject GenerateMonsterTrigger(GameObject sector, MonsterCluster cl)
        {
            Random rd = new Random();
            GameObject trigger = new GameObject("MonsterTrigger");

            MonsterTrigger mt = trigger.AddComponent<MonsterTrigger>();
            mt.Cluster = cl;
            trigger.transform.position = sector.transform.position + Vector3.up * cl.YOffset;

            trigger.transform.parent = this.transform;

            return trigger;
        }

        private GameObject GenerateSpeedTrigger(GameObject sector, float _sc)
        {
            GameObject trigger = new GameObject("SpeedTrigger");

            SpeedTrigger st = trigger.AddComponent<SpeedTrigger>();
            st.SpeedChange = _sc;
            trigger.transform.position = sector.transform.position - Vector3.up * 3f;

            trigger.transform.parent = this.transform;

            return trigger;
        }

        void OnDestroy()
        {
            Debug.Assert(_instance != null);
            _instance = null;
        }

        private void Update()
        {
            if (lastBossSectorTriggerList.TrueForAll(_ => _.GetComponent<MonsterTrigger>().Triggered == true) == false)
            {
                gameObject.transform.position += new Vector3(0, -FlowSpeed * Time.deltaTime, 0);
            }
            else
            {

                GameObject.Find("lboss_cannon").GetComponent<BossCannonAI>().BossEncountered = true;
            }
        }
    }
}
