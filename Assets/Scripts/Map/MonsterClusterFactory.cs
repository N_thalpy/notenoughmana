﻿using UnityEngine;

namespace NotEnoughMana.Map
{
    public static class MonsterClusterFactory
    {
        public static MonsterCluster ThanksGiving;
        public static MonsterCluster ScaryRush;
        public static MonsterCluster Boss;

        public static MonsterCluster RockRush;
        public static MonsterCluster ThreeGunman;
        public static MonsterCluster DroneLeader;

        public static MonsterCluster DroneParty;
        public static MonsterCluster NierDrone;

        public static MonsterCluster HitoNewby;
        public static MonsterCluster BytoNewby;

        public static MonsterCluster[] Cargo1;
        public static MonsterCluster[] Cargo2;
        public static MonsterCluster[] Cargo3;
        public static MonsterCluster[] Cargo4;
        public static MonsterCluster[] Cargo5;
        public static MonsterCluster[] Cargo6;
        public static MonsterCluster[] Cargo7;
        public static MonsterCluster[] Cargo8;
        public static MonsterCluster[] Cargo9;
        public static MonsterCluster[] Cargo10;
        public static MonsterCluster[] Cargo11;
        public static MonsterCluster[] Cargo12;
        public static MonsterCluster[] Cargo13;



        static MonsterClusterFactory()
        {
            GameObject ghost = Resources.Load<GameObject>("mob/ghost");
            GameObject turret = Resources.Load<GameObject>("mob/turret");
            GameObject turretC = Resources.Load<GameObject>("mob/turretCircle");
            GameObject shieldghost = Resources.Load<GameObject>("mob/shieldghost");
            GameObject popupshieldghost = Resources.Load<GameObject>("mob/popupshieldghost");
            GameObject droneFlw = Resources.Load<GameObject>("mob/LinkDroneFollower");
            GameObject droneLd = Resources.Load<GameObject>("mob/LinkDroneLeader");
            GameObject gunman = Resources.Load<GameObject>("mob/Gunman");
            GameObject rock = Resources.Load<GameObject>("mob/basic_big_rock");
            //GameObject lbossplatform = Resources.Load<GameObject>("mob/lboss_platform");
            GameObject golem = Resources.Load<GameObject>("mob/lavagolem1");
            GameObject bomb = Resources.Load<GameObject>("mob/ManaBomb");
            GameObject nierDrone = Resources.Load<GameObject>("mob/NierLeader");

            Cargo1 = new MonsterCluster[]
            {
                new MonsterCluster(new GameObject[]
                {
                    ghost, ghost
                }, -3f),
                new MonsterCluster(new GameObject[]
                {
                    turret, ghost
                }, 0f),
                new MonsterCluster(new GameObject[]
                {
                    ghost, turret
                }, 2f),
            };
            Cargo2 = new MonsterCluster[]
            {
                new MonsterCluster(new GameObject[]
                {
                    ghost, ghost
                }, -3f),
                new MonsterCluster(new GameObject[]
                {
                    droneLd
                }, -1f),
                new MonsterCluster(new GameObject[]
                {
                    ghost
                }, 0f),
                new MonsterCluster(new GameObject[]
                {
                    nierDrone
                }, 2f),
            };
            Cargo3 = new MonsterCluster[]
            {
                new MonsterCluster(new GameObject[]
                {
                    turret, turret
                }, -1f),
                new MonsterCluster(new GameObject[]
                {
                    nierDrone
                }, 0f),
            };
            Cargo4 = new MonsterCluster[]
            {
                new MonsterCluster(new GameObject[]
                {
                    gunman, ghost,
                }, -2f),
                new MonsterCluster(new GameObject[]
                {
                    gunman, droneLd
                }, -1f),
                new MonsterCluster(new GameObject[]
                {
                    ghost, droneLd
                }, 1f),
            };
            Cargo5 = new MonsterCluster[]
            {
                new MonsterCluster(new GameObject[]
                {
                    golem, shieldghost
                }, 0f),
                new MonsterCluster(new GameObject[]
                {
                    turretC,
                }, 1f),
                new MonsterCluster(new GameObject[]
                {
                    golem,
                }, 2f),
            };
            Cargo6 = new MonsterCluster[]
            {
                new MonsterCluster(new GameObject[]
                {
                    ghost, popupshieldghost, shieldghost
                }, 0f),
                new MonsterCluster(new GameObject[]
                {
                    popupshieldghost, shieldghost, ghost, ghost
                }, 1f),
                new MonsterCluster(new GameObject[]
                {
                    ghost, shieldghost, ghost
                }, 2f),
            };
            Cargo7 = new MonsterCluster[]
            {
                new MonsterCluster(new GameObject[]
                {
                    gunman, gunman
                }, 0f),
                new MonsterCluster(new GameObject[]
                {
                    turretC , gunman
                }, 1f),
                new MonsterCluster(new GameObject[]
                {
                    nierDrone
                }, 1.5f),
                new MonsterCluster(new GameObject[]
                {
                    popupshieldghost
                }, 2f),
            };
            Cargo8 = new MonsterCluster[]
            {
                new MonsterCluster(new GameObject[]
                {
                    golem, bomb
                }, -3f),
                new MonsterCluster(new GameObject[]
                {
                    golem, bomb
                }, -1.5f),
                new MonsterCluster(new GameObject[]
                {
                    turretC, bomb
                }, 0f),
                new MonsterCluster(new GameObject[]
                {
                    droneLd
                }, 0.5f)
            };
            Cargo9 = new MonsterCluster[]
            {
                new MonsterCluster(new GameObject[]
                {
                    golem, golem
                }, -3f),
                new MonsterCluster(new GameObject[]
                {
                    popupshieldghost
                }, -2.5f),
                new MonsterCluster(new GameObject[]
                {
                    ghost
                }, -2f),
                new MonsterCluster(new GameObject[]
                {
                    ghost
                }, -1.5f),
                new MonsterCluster(new GameObject[]
                {
                    ghost, turretC
                }, -1.0f),
                new MonsterCluster(new GameObject[]
                {
                    ghost, 
                }, 0.5f),
                new MonsterCluster(new GameObject[]
                {
                    popupshieldghost,
                }, 0f),
                new MonsterCluster(new GameObject[]
                {
                    nierDrone
                }, 0.5f),
                new MonsterCluster(new GameObject[]
                {
                    ghost,
                }, 1.0f),
                new MonsterCluster(new GameObject[]
                {
                    ghost, turret
                }, 1.5f),
            };

            HitoNewby = new MonsterCluster(new GameObject[]
            {
                ghost,
                ghost,
            }, 0f);

            BytoNewby = new MonsterCluster(new GameObject[]
            {
                turret,
                turret,
                ghost,
            }, 0f);

            ThanksGiving = new MonsterCluster(new GameObject[]
            {
                ghost,
                bomb,
                shieldghost,
                popupshieldghost,
                turret,
                droneLd,
                nierDrone,
                gunman,
                rock,
            }, 0f);
            ScaryRush = new MonsterCluster(new GameObject[]
            {
                ghost,
                ghost,
                ghost,
                ghost,
            }, 0);

            RockRush = new MonsterCluster(new GameObject[]
            {
                golem,
                golem,
                turretC,
            }, 3f);
            ThreeGunman = new MonsterCluster(new GameObject[]
            {
                gunman,
                gunman,
                gunman,
            }, -2f);
            
            Boss = new MonsterCluster(new GameObject[]
            {
                gunman,
            }, -1f);

            DroneLeader = new MonsterCluster(new GameObject[]
            {
                droneLd,

            }, -1f);

            NierDrone = new MonsterCluster(new GameObject[]
            {
                nierDrone,

            }, 0f);

            DroneParty = new MonsterCluster(new GameObject[]
            {
                droneLd,
                nierDrone,

            }, 0f);
        }
    }
}
