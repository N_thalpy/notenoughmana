﻿using System.Collections.Generic;
using UnityEngine;

namespace NotEnoughMana.Map
{
    public class InifiniteScroll : MonoBehaviour
    {
        private List<GameObject> cloneList;
        private float accumulate;

        public GameObject Prefab;
        public float UnitLength;
        public int CloneCount;
        public float Speed;

        private void Start()
        {
            accumulate = 0;

            cloneList = new List<GameObject>();
            for (int idx = 0; idx < CloneCount; idx++)
            {
                GameObject clone = GameObject.Instantiate(Prefab, transform.position + Vector3.up * UnitLength * idx, Prefab.transform.rotation);
                clone.transform.parent = this.transform;
                cloneList.Add(clone);
            }
        }
        private void Update()
        {
            float dx = Speed * Time.deltaTime;
            if (accumulate + dx > UnitLength)
                dx -= UnitLength;
            if (accumulate + dx < 0)
                dx += UnitLength;
            accumulate += dx;

            for (int idx = 0; idx < cloneList.Count; idx++)
                cloneList[idx].transform.position -= Vector3.up * dx;
        }
    }
}
