﻿using NotEnoughMana.Mob;
using UnityEngine;

namespace NotEnoughMana.Map
{
    public sealed class SpeedTrigger : MonoBehaviour
    {
        private bool triggered;

        public float SpeedChange;
        public bool Triggered
        {
            get
            {
                return triggered;
            }
        }

        private void Awake()
        {
            triggered = false;
        }
        private void Update()
        {
            if (transform.position.y < 0 && triggered == false)
            {
                MapGenerator.Instance.FlowSpeed = SpeedChange;

                triggered = true;
            }
        }
    }
}
