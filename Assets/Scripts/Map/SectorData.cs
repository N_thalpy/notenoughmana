﻿using UnityEngine;

namespace NotEnoughMana.Map
{
    public sealed class SectorData
    {
        public SectorEnum SectorEnum;
        public GameObject ResourcePrefab;
        public MonsterCluster[] MonsterClusterList;

        public int MonsterCount;

        public float SpeedChange;

        public float Height;
    }
}
