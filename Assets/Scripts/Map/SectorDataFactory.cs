﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Random = System.Random;

namespace NotEnoughMana.Map
{
    public static class SectorDataFactory
    {
        private static List<SectorData> dataList;

        static SectorDataFactory()
        {
            dataList = new List<SectorData>()
            {
                new SectorData()
                {
                    SectorEnum = SectorEnum.Empty,
                    ResourcePrefab = Resources.Load<GameObject>("background/cargo_prefab"),
                    MonsterClusterList = new MonsterCluster[0],
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo,
                    ResourcePrefab = Resources.Load<GameObject>("background/cargo_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo1,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo2,
                    ResourcePrefab = Resources.Load<GameObject>("background/normal_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo2,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo3,
                    ResourcePrefab = Resources.Load<GameObject>("background/normal_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo3,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo4,
                    ResourcePrefab = Resources.Load<GameObject>("background/cargo_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo4,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo5,
                    ResourcePrefab = Resources.Load<GameObject>("background/cargo_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo5,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo6,
                    ResourcePrefab = Resources.Load<GameObject>("background/normal_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo6,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo7,
                    ResourcePrefab = Resources.Load<GameObject>("background/normal_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo7,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                 new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo8,
                    ResourcePrefab = Resources.Load<GameObject>("background/cargo_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo8,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                  new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo9,
                    ResourcePrefab = Resources.Load<GameObject>("background/cargo_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo9,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                   new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo10,
                    ResourcePrefab = Resources.Load<GameObject>("background/normal_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo10,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                    new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo11,
                    ResourcePrefab = Resources.Load<GameObject>("background/normal_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo11,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                     new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo12,
                    ResourcePrefab = Resources.Load<GameObject>("background/cargo_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo12,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                      new SectorData()
                {
                    SectorEnum = SectorEnum.Cargo13,
                    ResourcePrefab = Resources.Load<GameObject>("background/cargo_prefab"),
                    MonsterClusterList = MonsterClusterFactory.Cargo13,
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
                new SectorData()
                {
                    SectorEnum = SectorEnum.Explosive,
                    ResourcePrefab = Resources.Load<GameObject>("background/boss_platform"),
                    MonsterClusterList = new MonsterCluster[]
                    {
                        MonsterClusterFactory.DroneLeader,
                    },
                    MonsterCount = 1,
                    SpeedChange = 1f,
                    Height = 9.3f,
                },
            };
        }

        public static SectorData GetRandomDataFromEnum(SectorEnum enumData)
        {
            Random rd = new Random();
            return dataList.Where(_ => _.SectorEnum == enumData).OrderBy(_ => rd.NextDouble()).First();
        }
    }
}
