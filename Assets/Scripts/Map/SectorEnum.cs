﻿namespace NotEnoughMana.Map
{
    public enum SectorEnum
    {
        None,

        Cargo,
        Cargo2,
        Cargo3,
        Cargo4,
        Cargo5,
        Cargo6,
        Cargo7,
        Cargo8,
        Cargo9,
        Cargo10,
        Cargo11,
        Cargo12,
        Cargo13,
        Cargo14,
        Cargo15,
        Cargo16,
        Cargo17,
        Cargo18,
        Empty,
        Explosive,
        FirstRateRoom,
        SecondRateRoom,
    }
}
