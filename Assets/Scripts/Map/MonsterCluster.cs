﻿using UnityEngine;

namespace NotEnoughMana.Map
{
    public sealed class MonsterCluster
    {
        public GameObject[] MonsterList;
        public float YOffset;

        public MonsterCluster(GameObject[] list, float yOffset)
        {
            MonsterList = list;
            YOffset = yOffset;
        }
    }
}
